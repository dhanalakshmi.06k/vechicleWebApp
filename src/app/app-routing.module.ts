import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AssetListComponent} from './asset-list/asset-list.component'
import {LoginComponent} from "./login/login.component";
import {SettingsComponent} from "./settings/settings.component";
import {ReportComponent} from "./report/report/report.component";
import {LessorTypeComponent} from "./lessor-type/lessor-type.component";
import {ShiftTypeCostComponent} from "./shift-type-cost/shift-type-cost.component";
import {ExpenseTypeComponent} from "./expense-type/expense-type.component";
import {FuelTypeComponent} from "./fuel-type/fuel-type.component";
import {ServiceTypeComponent} from "./service-type/service-type.component";
import {VechicleHistoryComponent} from "./vechicle-history/vechicle-history.component";
import {UserManagementComponent} from "./user-management/user-management.component";
import {ForgotpasswordComponent} from "./forgotpassword/forgotpassword.component";
import {VehicleExpenseComponent} from "./vehicle-expense/vehicle-expense.component";
import {VechicleTransactionComponent} from "./vechicle-transaction/vechicle-transaction.component";
import {VechicleTypeComponent} from "./vechicle-type/vechicle-type.component";
import {VehicleCorporateContractComponent} from "./companyAssociation/vehicle-corporate-contract/vehicle-corporate-contract.component";
import {CorporateContract1Component} from "./corporate-contract1/corporate-contract1.component";
import {CorporateContract2Component} from "./corporateContractTransaction/corporate-contract2/corporate-contract2.component";
import {CorporateContract3Component} from "./corporate-contract3/corporate-contract3.component";
import {CorporateContract4Component} from "./corporate-contract4/corporate-contract4.component";
import {CorporateContract5Component} from "./corporate-contract5/corporate-contract5.component";
import {CorporateContract6Component} from "./corporate-contract6/corporate-contract6.component";
import {CorporateContract7Component} from './corporate-contract7/corporate-contract7.component';
import {OnDemandCorporateContractComponent} from './corporateContractTransaction/on-demand-corporate-contract/on-demand-corporate-contract.component';
import {OnDemandCompanyContractComponent} from "./on-demand-company-contract/on-demand-company-contract.component";
import {OnDemandshiftTypeComponent} from './corporateContractTransaction/on-demandshift-type/on-demandshift-type.component';
import {VehicleCorporteAssociation1Component} from "./companyAssociation/vehicle-corporte-association1/vehicle-corporte-association1.component";
import {VehicleSiftCorporteAssociation2Component} from "./companyAssociation/vehicle-sift-corporte-association2/vehicle-sift-corporte-association2.component";
import {CorporatContractFixedDayKmContractComponent} from "./corporat-contract-fixed-day-km-contract/corporat-contract-fixed-day-km-contract.component";
import {OnDemandAssociationComponent} from "./companyAssociation/on-demand-association/on-demand-association.component";
import {CompanyCorporateContract2Component} from './companyAssociation/company-corporate-contract2/company-corporate-contract2.component';
import {CompanyAssetComponent} from "./dashboard/company-asset/company-asset.component";
import {DriverAssetComponent} from "./dashboard/driver-asset/driver-asset.component";
import {LesserAssetComponent} from "./dashboard/lesser-asset/lesser-asset.component";
import {VehicleAssetComponent} from "./dashboard/vehicle-asset/vehicle-asset.component";
import {CompanyWiseSummaryComponent} from "./report/company-wise-summary/company-wise-summary.component";
import {OnDemandUsageReportComponent} from "./report/on-demand-usage-report/on-demand-usage-report.component";
import {LesserOndemandAssociationComponent} from "./lesserAssociation/lesser-ondemand-association/lesser-ondemand-association.component";
import {LsserShiftTypeAssoicationComponent} from "./lesserAssociation/lsser-shift-type-assoication/lsser-shift-type-assoication.component";
import {Lessercc2TypeAssocationComponent} from "./lesserAssociation/lessercc2-type-assocation/lessercc2-type-assocation.component";
import {LesserCommonTypeAssociationComponent} from "./lesserAssociation/lesser-common-type-association/lesser-common-type-association.component";
import {LesserReportComponent} from "./report/lesser-report/lesser-report.component";
import {CompanyLesserHeaderComponent} from "./companyLesserAssociationHeader/company-lesser-header/company-lesser-header.component";
import {Lessercc1TypeAssociationComponent} from "./lesserAssociation/lessercc1-type-association/lessercc1-type-association.component";


const routes: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },

    {path: 'companyAsset', component: CompanyAssetComponent},
    {path: 'lesserAsset', component: LesserAssetComponent},
    {path: 'DriverAsset', component: DriverAssetComponent},
    {path: 'vehicleAsset', component: VehicleAssetComponent},
    {path: 'VehicleCorporateContract1', component: VehicleCorporteAssociation1Component},
    {path: 'companyCorporateContract2', component: CompanyCorporateContract2Component},


    {path: 'onDemand', component: OnDemandCorporateContractComponent},

    {path: 'cc1', component: CorporateContract1Component},
    {path: 'companyOnDemandAssoication', component: OnDemandAssociationComponent},
    {path: 'cc2', component: CorporateContract2Component},
    {path: 'cc3', component: CorporateContract3Component},
    {path: 'cc4', component: CorporateContract4Component},
    {path: 'cc5', component: CorporateContract5Component},
    {path: 'cc6', component: CorporateContract6Component},
    {path: 'cc7', component: CorporateContract7Component},



    {path: 'asset', component: AssetListComponent},
    {path: 'LessorType', component: LessorTypeComponent},
    {path: 'ShiftTypeCost', component: ShiftTypeCostComponent},
    {path: 'ExpenseType', component: ExpenseTypeComponent},
    {path: 'FuelType', component: FuelTypeComponent},
    {path: 'vehicleType', component: VechicleTypeComponent},

    {path: 'ServiceType', component: ServiceTypeComponent},
    {path: 'vehicleHistory', component: VechicleHistoryComponent},
    {path: 'userManagement', component: UserManagementComponent},
    {path: 'vehicleExpense', component: VehicleExpenseComponent},
    {path: 'forgotPassword', component: ForgotpasswordComponent},


    {path: 'companyLesserAssoicationHeader', component: CompanyCorporateContract2Component},
    {path: 'vechicleTransaction', component: VechicleTransactionComponent},
    {path: 'CompanyCctransaction', component: CorporateContract1Component},
    {path: 'login', component: LoginComponent},


    {path: 'OnDemandShiftType', component: OnDemandshiftTypeComponent},


    {path: 'VehicleCorporateContractShiftType2', component: VehicleSiftCorporteAssociation2Component},
    {path: 'vehicleCorporateContractCommon', component: VehicleCorporateContractComponent},
    {path: 'vehicleCorporatContractFixedDayKm', component: CorporatContractFixedDayKmContractComponent},

    {path: 'report', component: ReportComponent},

    {path: 'companyWiseReport', component: CompanyWiseSummaryComponent},
    {path: 'onDemandReport', component: OnDemandUsageReportComponent},
    {path: 'lesserOnDemandAssociation', component: LesserOndemandAssociationComponent},
    {path: 'lesserShiftTypeAssociation', component: LsserShiftTypeAssoicationComponent},
    {path: 'lesserCommonTypeAssociation', component: LesserCommonTypeAssociationComponent},
    {path: 'lessercc2TypeAssociation', component: Lessercc2TypeAssocationComponent},
    {path: 'lessercc1TypeAssociation', component: Lessercc1TypeAssociationComponent},
    {path: 'lesserReport', component: LesserReportComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
