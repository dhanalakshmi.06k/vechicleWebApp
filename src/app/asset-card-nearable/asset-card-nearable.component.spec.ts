import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AssetCardNearableComponent} from './asset-card-nearable.component';

describe('AssetCardNearableComponent', () => {
    let component: AssetCardNearableComponent;
    let fixture: ComponentFixture<AssetCardNearableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AssetCardNearableComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AssetCardNearableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
