import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

import {AssetService} from "../../services/asset.service";
import * as _ from 'lodash';

declare var $: any;

@Component({
    selector: 'app-asset-card',
    templateUrl: './asset-card.component.html',
    styleUrls: ['./asset-card.component.scss']
})
export class AssetCardComponent implements OnChanges, OnInit {
    public id: any;
    public assetLinkedDetails: any;
    @Input() isCardClickable: boolean = false;
    @Input() mainStatusAsset: boolean
    @Input() assetDetailsAfterLinkingUpdated: any;
    @Input() assetData: any;
    @Input() upadtedCartSpecificDetails: any;
    @Input() showGroupBtn: boolean;
    @Input() showCloseBtn: boolean;
    @Input() showLinkBtn: boolean;
    @Output() editAsset: EventEmitter<any> = new EventEmitter();
    @Output() delinkAssetDetails: EventEmitter<any> = new EventEmitter();
    @Output() linkAssetDetails: EventEmitter<any> = new EventEmitter();

    constructor(public assetService: AssetService) {
    }

    ngOnInit() {
        this.getAssetsLinkedDetails(this.assetData);
    }

    ngOnChanges() {
        if (this.assetDetailsAfterLinkingUpdated) {
            this.checkToUpdatedAssetLinkCount();
        }
        if (this.upadtedCartSpecificDetails) {
            if (this.assetData._id === this.upadtedCartSpecificDetails._id) {
                this.assetService.getAssetDetailsByMongodbId(this.upadtedCartSpecificDetails._id)
                    .subscribe(assetDetails => {
                        this.assetData = assetDetails
                    });

            }

        }
    }


    checkToUpdatedAssetLinkCount() {
        if (this.assetData._id === this.assetDetailsAfterLinkingUpdated._id) {
            this.getAssetsLinkedDetails(this.assetDetailsAfterLinkingUpdated);
        }
    }

    editAssetDetails() {
        this.id = this.assetData._id;
        this.editAsset.emit(this.assetData);
    }

    closeBtnClicked() {
        this.delinkAssetDetails.emit(this.assetData);
    }

    linkBtnClicked() {
        this.linkAssetDetails.emit(this.assetData);
    }

    getAssetsLinkedDetails(assetDetails) {
        let assetLinkedData: any;
        assetLinkedData = _.groupBy(assetDetails.assetsLinked, function (asset) {
            return asset.assetType;
        });
        this.assetLinkedDetails = assetLinkedData;
    }


    getBoxShadowForCard(type) {
        switch (type) {
            case 'car':
                return '0 2px 5px 0 rgba(92, 107, 192), 0 2px 10px 0 rgba(92, 107, 192)';
            case 'obdDevice':
                return '0 2px 5px 0 rgba(38, 166, 154), 0 2px 10px 0 rgba(38, 166, 154)';
            case 'driver':
                return '0 2px 5px 0 rgba(102, 187, 106), 0 2px 10px 0 rgba(102, 187, 106)';
            case 'beacon':
                return '0 2px 5px 0 rgba(66, 165, 245), 0 2px 10px 0 rgba(66, 165, 245)';
            case 'rfId':
                return '0 2px 5px 0 rgba(220, 120, 107), 0 2px 10px 0 rgba(220, 120, 107)';
        }
    }


}
