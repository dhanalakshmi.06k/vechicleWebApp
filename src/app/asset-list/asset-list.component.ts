import {Component, EventEmitter, OnInit, Output} from '@angular/core';

import {AssetService} from '../../services/asset.service';
import {AssetCountType, AssetTypes, AssetTypeWithCount, Count} from './asset-count-type';
import {Router} from "@angular/router";
import {FormService} from "../../services/form.service";
import * as _ from 'lodash';
import {AssetCommunicationService} from "../../services/asset-communication.service";
import {Subscription} from "rxjs";

declare var $: any;

@Component({
    selector: 'app-asset-list',
    templateUrl: './asset-list.component.html',
    styleUrls: ['./asset-list.component.scss']
})
export class AssetListComponent implements OnInit {

    currRoute: string;
    assetFromConfig: any;
    public assetCountByType;
    public mainAsset: any;
    public showSlider: boolean = false;
    public assetDetailsAfterLinkingUpdated: any;
    public allAssets: any = [];
    public assetTypesWithLabels: any = [];
    public assetCount: any = [];
    public selectedAssets: any = [];
    public assetType: string;
    public assetSelectedType: any;
    public assetId: any;
    public assetLinkedDetails: any;
    public delinkAssetDetails: any;
    public linkAssetDetails: any;
    public selectedPageCount: any;
    public totalPageCount: any;
    public currentPage: any = 1;
    public upadtedCartSpecificDetails: any;
    public skip: number;
    public limit: number;
    public isShowAsset: boolean;
    public pagination: any;
    public countObj: AssetCountType;
    p: number = 1;
    subscription: Subscription;
    public RECORDS_PER_PAGE = 12
    @Output() event: EventEmitter<string> = new EventEmitter<string>()

    constructor(public assetService: AssetService, private router: Router, public formService: FormService,
                private assetCommunicationService:AssetCommunicationService) {
        this.assetCommunicationService.getAssetRefresh().subscribe(assetDetails => {
            console.log("inside 00000000000000000 list")
            this.setAssetListByTypes("", 0, this.pagination.recordsPerPage)


        });

        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };

        this.currRoute = router.url;
    }



    filterAssetOnType(assetType) {
        let res;
        res = _.find(this.selectedAssets, function (o) {
            return o === assetType;
        });
        if (res) {
            let event = _.remove(this.selectedAssets, function (n) {
                return (n === res);
            });
        } else {
            this.selectedAssets.push(assetType);
        }
        this.getAssetForSelectedAssetTypes(0, this.RECORDS_PER_PAGE);
    }

    getAssetForSelectedAssetTypes(skip, limit) {
        this.isShowAsset = true
        this.currentPage = 1;
        let selectedAssetsStr: string = '';
        selectedAssetsStr = this.selectedAssets.toString();
        this.getAssetCountByTypes(selectedAssetsStr);
        this.setAssetListByTypes(selectedAssetsStr, skip, limit);
    }

    getDataByPageNo(page) {
        //this.currentPage=page;
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.RECORDS_PER_PAGE;
        } else {
            this.skip = (page - 1) * this.RECORDS_PER_PAGE;
            this.limit = this.RECORDS_PER_PAGE;
        }
        if (this.selectedAssets.length > 0) {
            this.setAssetListByTypes(this.selectedAssets.toString(), this.skip, this.limit);
        }
        if (this.selectedAssets.length == 0) {
            this.setAssetListByTypes(null, this.skip, this.limit);
            this.setAssetListByTypes(null, this.skip, this.limit);
        }
    }

    assetTypeToAdd(assetType) {
        this.mainAsset = true;
        this.assetSelectedType = assetType;
    }

    savedAssetCardToAssetList(savedAssetValue) {
        // this.showMessagePopup('savedAssetSuccessfully');
        //this.selectedAssets = [];
        //this.getAssetTotalCountForPagination();
        //this.getFirstFewAssets();
        this.getAssetForSelectedAssetTypes(0, this.pagination.recordsPerPage);
        this.getAssetIndividualCounts();
    }

    syncUpdateAssetCart(dataUpdated) {
        this.upadtedCartSpecificDetails = dataUpdated;
        /*  this.showMessagePopup('updatedAssetSuccessfully');*/
    }

    getAssetIndividualCounts() {
        this.assetService.getAssetCount()
            .subscribe((assetIndividualCount: AssetCountType) => {
                this.assetCountByType = assetIndividualCount['count'];
                this.assetCountByType = _.orderBy(this.assetCountByType, ['order'], ['asc']);
                for (let assetCount of this.assetCountByType) {
                    for (let selectedAsset of this.selectedAssets) {
                        if (assetCount.type === selectedAsset) {
                            assetCount.selectedForFiler = true;
                            break;
                        } else {
                            assetCount.selectedForFiler = false;
                        }
                    }
                }
            });
    }

    editAsset(assetDetails) {
        console.log("****************")
        console.log(assetDetails)
        this.showSlider = true;
        this.assetId = assetDetails._id;
        this.assetType = assetDetails.assetType;
        // this.getAssetLinked(this.assetId);
    }

    private getAssetCountByTypes(selectedAssetsStr: string) {
        this.assetService.getCountByTypes(selectedAssetsStr)
            .subscribe(result => {
                this.totalPageCount = result["count"];
            });
    }

    private setAssetListByTypes(selectedAssetsStr: string, skip, limit) {
        this.assetService.getAllByType(selectedAssetsStr, skip, limit)
            .subscribe(result => {

                this.isShowAsset = false
                this.allAssets = result;
            },
                error => {
                    this.allAssets = error;
                });
    }

    ngOnInit() {
        this.isShowAsset = true
        this.assetService.getAssetPaginationCount()
            .subscribe((allAssetCount: any) => {
                this.totalPageCount = allAssetCount.count;
            });
        this.assetService.getAllByType("Lesser", 0, 10)
            .subscribe(result => {

                    this.isShowAsset = false
                    this.allAssets = result;
                },
                error => {
                    this.allAssets = error;
                });

        this.assetService.getAllAssetTypes()
            .subscribe((assetTypes: Array<AssetTypes>) => {
                this.assetTypesWithLabels = assetTypes;
                this.assetService.getAssetCount()
                    .subscribe((assetIndividualCount: AssetCountType) => {
                        let assetCountByType: Array<Count>;
                        assetCountByType = assetIndividualCount.count;
                        let newArray: any = [];
                        let succ;
                        let obj: AssetTypeWithCount;
                        _.forEach(assetTypes, function (value) {
                            console.log(value);
                            succ = _.find(assetCountByType, function (o) {
                                return o.type === value.type;
                            });
                            if (succ) {
                                obj = {
                                    'label': value.label,
                                    'type': value.type,
                                    'count': succ.count,
                                }
                                newArray.push(obj);
                            }
                        });
                        this.assetCountByType = newArray;
                    });
            });

    }
}
