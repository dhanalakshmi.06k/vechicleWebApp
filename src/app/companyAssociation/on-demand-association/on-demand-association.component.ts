import { Component, OnInit } from '@angular/core';
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import {AssetService} from "../../../services/asset.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-on-demand-association',
  templateUrl: './on-demand-association.component.html',
  styleUrls: ['./on-demand-association.component.scss']
})
export class OnDemandAssociationComponent implements OnInit {
    allVechicleTypeDropDown:any;
    formConfigData:any={};
    term:any;
    vechicleDropdownList:any;
    lesserDropdownList:any;
    corporateContractType="OnDemand";
    corporateContractFields:any=[];
    corporateContractDetailsList:any=[];
    currentCompanySelected:string;
    companyAssetDropDown:any;
    companySelected:any;
    vehicleTypeSelected:any;
    public pagination: any;
    isEdit=false
    companyDropdownSettings:any = {}
   vehicleTypeDropdownSettings:any = {}

    public skip: number;
    public limit: number;
    constructor(public vechicleTypeServiceService:VechicleTypeServiceService,
                public corporateContractFieldsService:CorporateContractFieldsService,
                public assetService:AssetService,
                public vehicleCorporateContractService:VehicleCorporateContractService,
                private router: Router) {
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
        this.corporateContractType="OnDemand"
       this.companyDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.vehicleTypeDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'vehicleTypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }

    }

    onCompanySelectSelect(item: any) {

        this.currentCompanySelected=item;
        console.log(this.currentCompanySelected);

    }

    onSelectAll (items: any) {
        console.log(items);
    }
    onItemDeSelect(items:any){
        console.log(items)
    }
    submitVehicleCorporateContractDetails(){
      this.formConfigData['companyName']= this.companySelected[0].companyName
                        this.formConfigData['vehicleType']= this.vehicleTypeSelected[0].vehicleTypeName
                        this.formConfigData['corporateContract']= this.corporateContractType
      this.vehicleCorporateContractService.checkCCExitsBasedOnVehicleTypeAndName(this.formConfigData['companyName']
                 ,this.corporateContractType,this.formConfigData['vehicleType'] )
                 .subscribe((existResponse: any) => {

                     if(existResponse.length==0){
                         this.vehicleCorporateContractService.saveVehicleCorporateContract( this.formConfigData )
                             .subscribe((ccCount: any) => {
                                 console.log("**************after saveeeeeee****checkCCExitsBasedOnVehicleTypeAndNamessssss*************");
                                 console.log( ccCount)
                                 this.getCorporateContractTotalCountForPagination();
                                 this.getCorporateContractByType(this.corporateContractType,0,10)
                             });
                     }
                     else{
                         alert(this.companySelected[0].companyName + "is already configured to OnDemand")
                     }
                 });

    }

    editCorporateContract(corporateContractDetails){
        console.log( corporateContractDetails['companyName'])
        this.isEdit=true
        this.formConfigData['agreementStartDate']=corporateContractDetails['agreementStartDate']
        this.formConfigData['agreementEndDate']=corporateContractDetails['agreementEndDate']
        this.formConfigData=corporateContractDetails
        console.log("**************formConfigData*****ccCount************");
        console.log( this.formConfigData)

    }

    getFromSetUpDetails(){
        this.vehicleTypeSelected=""
        this.companySelected=[];
        this.vehicleTypeSelected={}
        this.formConfigData={};
    }
    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)
                this.allVechicleTypeDropDown=vechicleTypes
            })
    }



    companyOnDemandAssoication(){
        this.router.navigate(['companyOnDemandAssoication']);
    }
    populateCorporateContractType(){
        this.isEdit=false
       this.getAllVechicleType()
        this.getAllCompanyAsset();

        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }
            else{
                this.corporateContractFields=[]
            }
        }

    }


    updateVehicleCorporateContractDetails(){
        this.vehicleCorporateContractService.updateVehicleCorporateContractById(this.formConfigData['_id'],this.formConfigData)
            .subscribe(expenseData => {
                console.log("**************vehicleCorporateContractService updates*****************");

            });
    }

    deleteCCDetails(CCDetails){
        this.vehicleCorporateContractService.deleteVehicleCorporateContractByMongodbId(CCDetails['_id'])
            .subscribe(expenseData => {
                this.getCorporateContractByType(this.corporateContractType,0,10)
                this.getCorporateContractCount();
            });
    }

deleteTransactionData:any
intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }

    getCorporateContractTotalCountForPagination() {
        this.vehicleCorporateContractService.getVehicleCorporateContractCount()
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }
    getCorporateContractBasedOnrange(skip, limit) {
        this.vehicleCorporateContractService.getAllVehicleCorporateContractList()
            .subscribe((corporateContractDetails:any) => {
                console.log("----------typeof corporateContractDetails")
                console.log( corporateContractDetails)
                console.log(typeof corporateContractDetails)
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList
            })
    }

    getAllLesserAsset(){
        this.assetService.getAssetlistSpecificList("Lesser")
            .subscribe((lesserList: any) => {
                this.lesserDropdownList =lesserList
            })
    }

    getAllCompanyAsset(){
        this.assetService.getAssetlistSpecificList("Company")
            .subscribe((companyList: any) => {
                this.companyAssetDropDown =companyList
            })
    }

    getCorporateContractByType(ccType,skip,limit) {
        this.vehicleCorporateContractService.getCorporateContractByCCTypeRange(ccType,skip,limit)
            .subscribe((corporateContractDetails:any) => {
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }

    getCorporateContractCount() {
        this.vehicleCorporateContractService.getCorporateContractByCCTypeCount(this.corporateContractType)
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getCorporateContractByType(this.corporateContractType,0,10)
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getCorporateContractByType(this.corporateContractType,0,10)
        }
    }


    ngOnInit() {
        this.getCorporateContractByType(this.corporateContractType,0,10)
        this.getCorporateContractCount();
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
    }

}
