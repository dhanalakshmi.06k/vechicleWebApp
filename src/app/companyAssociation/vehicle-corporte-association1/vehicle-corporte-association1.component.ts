import { Component, OnInit } from '@angular/core';
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import {AssetService} from "../../../services/asset.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-vehicle-corporte-association1',
  templateUrl: './vehicle-corporte-association1.component.html',
  styleUrls: ['./vehicle-corporte-association1.component.scss']
})
export class VehicleCorporteAssociation1Component implements OnInit {
    allVechicleTypeDropDown:any;
    formConfigData:any={};
    term:any;
    vechicleDropdownList:any;
    lesserDropdownList:any;
    corporateContractType="contract type 1";
    corporateContractFields:any=[];
    corporateContractDetailsList:any=[];
    currentCompanySelected:string;
    companyAssetDropDown:any;
    companySelected:any;
    vehicleTypeDropdownSettings:any={};
    vehicleTypeSelected:any;
    lesserTypeSelected:any;
    public pagination: any;
    isEdit=false

    public companyDropdownSettings = {};

    public vehicleDropdownSettings = {};

    public lesserDropdownSettings = {};


    constructor(public vechicleTypeServiceService:VechicleTypeServiceService,
                public corporateContractFieldsService:CorporateContractFieldsService,
                public assetService:AssetService,
                public vehicleCorporateContractService:VehicleCorporateContractService,
                private router: Router) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
        this.corporateContractType="contract type 1"
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)


    }

    onCompanySelectSelect(item: any) {

        this.currentCompanySelected=item;
        console.log(this.currentCompanySelected);

    }

    onSelectAll (items: any) {
        console.log(items);
    }
    onItemDeSelect(items:any){
        console.log(items)
    }


    submitVehicleCorporateContractDetails(){


                    this.formConfigData['companyName']= this.companySelected[0].companyName
                    this.formConfigData['vehicleType']= this.vehicleTypeSelected[0].vehicleTypeName
                    this.formConfigData['corporateContract']= this.corporateContractType



       this.vehicleCorporateContractService.checkCCExitsBasedOnVehicleTypeAndName(this.formConfigData['companyName']
            ,this.corporateContractType,this.formConfigData['vehicleType'] )
            .subscribe((existResponse: any) => {
                console.log("**************formConfigData****checkCCExitsBasedOnVehicleTypeAndNamessssss*************");
                    console.log(this.formConfigData)
                if(existResponse.length==0){
                    this.vehicleCorporateContractService.saveVehicleCorporateContract( this.formConfigData )
                        .subscribe((ccCount: any) => {
                            console.log("**************after saveeeeeee****checkCCExitsBasedOnVehicleTypeAndNamessssss*************");
                            console.log( ccCount)
                            this.getCorporateContractTotalCountForPagination();
                            this.getCorporateContractByType(this.corporateContractType)
                        });
                }
                else{
                    alert(this.companySelected[0].companyName + "is already configured to cc1")
                }
            });

    }

    editCorporateContract(corporateContractDetails){

        console.log( corporateContractDetails['companyName'])
        this.isEdit=true
        this.formConfigData['agreementStartDate']=corporateContractDetails['agreementStartDate']
        this.formConfigData['agreementEndDate']=corporateContractDetails['agreementEndDate']
        this.formConfigData=corporateContractDetails

    }

    getFromSetUpDetails(){
        this.formConfigData={}

        this.vehicleTypeSelected=""
        this.companySelected=[];
        this.vehicleTypeSelected={}


        this.companyDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.vehicleTypeDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'vehicleTypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }

    }
    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)
                this.allVechicleTypeDropDown=vechicleTypes
            })
    }


    companyOnDemandAssoication(){
        this.router.navigate(['companyOnDemandAssoication']);
    }
    populateCorporateContractType(){
        this.formConfigData={}
        this.isEdit=false
        this.companyDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.vehicleTypeDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'vehicleTypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }



        this.getAllLesserAsset()
        this.getAllVechicleType()
        this.getAllCompanyAsset();
        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }
            else{
                this.corporateContractFields=[]
            }
        }

    }


    updateVehicleCorporateContractDetails(){
        this.vehicleCorporateContractService.updateVehicleCorporateContractById(this.formConfigData['_id'],this.formConfigData)
            .subscribe(expenseData => {
                console.log("**************update message*****************");
                console.log( expenseData)
            });
    }

    deleteExpenseDetails(CCDetails){
        this.vehicleCorporateContractService.deleteVehicleCorporateContractByMongodbId(CCDetails['_id'])
            .subscribe(expenseData => {

                this.getCorporateContractTotalCountForPagination();
                this.getCorporateContractByType(this.corporateContractType)
            });
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }

    getCorporateContractTotalCountForPagination() {
        this.vehicleCorporateContractService.getVehicleCorporateContractCount()
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }
    getCorporateContractBasedOnrange(skip, limit) {
        this.vehicleCorporateContractService.getAllVehicleCorporateContractList()
            .subscribe((corporateContractDetails:any) => {
                console.log("----------typeof corporateContractDetails")
                console.log( corporateContractDetails)
                console.log(typeof corporateContractDetails)
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList
            })
    }


    getAllLesserAsset(){
        this.assetService.getAssetlistSpecificList("Lesser")
            .subscribe((lesserList: any) => {
                this.lesserDropdownList =lesserList
            })
    }

    getAllCompanyAsset(){
        this.assetService.getAssetlistSpecificList("Company")
            .subscribe((companyList: any) => {
                this.companyAssetDropDown =companyList
            })
    }

    getCorporateContractByType(ccType) {
        this.vehicleCorporateContractService.getVehicleCorporateContractByCCType(ccType)
            .subscribe((corporateContractDetails:any) => {
                console.log("----------typeof corporateContractDetails")
                console.log( corporateContractDetails)
                console.log(typeof corporateContractDetails)
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }

    ngOnInit() {
        this.getCorporateContractByType(this.corporateContractType)
        this.getCorporateContractTotalCountForPagination();
       this.populateCorporateContractType()
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)

    }

}
