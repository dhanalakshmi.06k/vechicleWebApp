import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-company-lesser-header',
  templateUrl: './company-lesser-header.component.html',
  styleUrls: ['./company-lesser-header.component.scss']
})
export class CompanyLesserHeaderComponent implements OnInit {

    constructor(public router:Router) { }

    ngOnInit() {
    }


    navigatecompanyCorporateContract() {
        this.router.navigate(['VehicleCorporateContract1']);
    }
    navigateOnDemandAssociation() {
        this.router.navigate(['lessercc1TypeAssociation']);
    }

}
