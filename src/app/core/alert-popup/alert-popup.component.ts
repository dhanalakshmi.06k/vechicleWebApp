import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-alert-popup',
    templateUrl: './alert-popup.component.html',
    styleUrls: ['./alert-popup.component.scss']
})
export class AlertPopupComponent implements OnInit {
    @Output() confirmedDelink: EventEmitter<any> = new EventEmitter();
    @Output() confirmedlink: EventEmitter<any> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    confirmed() {
        this.confirmedDelink.emit('OK');
    }

    linkingConfirmed() {
        this.confirmedlink.emit('OK');
    }

}
