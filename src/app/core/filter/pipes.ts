import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], term): any {
       /* console.log('term', term);*/

        return term
            ? items.filter(item => item.companySelected.companyName.indexOf(term) !== -1)
            : items

    }

 /*   transform(items: any[], searchText): any[] {
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toLowerCase();
        let list:any
        return items.filter( it => {
                if (it.companySelected.companyName) {
                    list=it.companySelected.companyName.toLowerCase().includes(searchText);
                }
                if (it.vehicleTypeSelected.vehicleTypeName) {
                    console.log("inside iffffffff===============.vehicleTypeName")
                    console.log(searchText)
                    console.log(it.vehicleTypeSelected.vehicleTypeName.toLowerCase().includes(searchText))
                    list= it.vehicleTypeSelected.vehicleTypeName.toLowerCase().includes(searchText);
                }
                if (it.corporateContract) {
                    list= it.corporateContract.toLowerCase().includes(searchText);
                }

                return list;


        });
    }*/
}

@Pipe({
    name: 'sortBy'
})
export class SortByPipe implements PipeTransform {
    transform(items: any[], sortedBy: string): any {
        console.log('sortedBy', sortedBy);

        return items.sort((a, b) => {return b[sortedBy] - a[sortedBy]});
    }
}