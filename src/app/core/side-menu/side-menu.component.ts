import {Component, OnInit} from '@angular/core';
import {menus} from './menu-element';

@Component({
    selector: 'app-side-menu',
    templateUrl: './side-menu.component.html',
    styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
    public menus = menus;

    constructor() {
    }

    ngOnInit() {
    }

}
