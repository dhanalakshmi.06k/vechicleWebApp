import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporatContractFixedDayKmContractComponent } from './corporat-contract-fixed-day-km-contract.component';

describe('CorporatContractFixedDayKmContractComponent', () => {
  let component: CorporatContractFixedDayKmContractComponent;
  let fixture: ComponentFixture<CorporatContractFixedDayKmContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporatContractFixedDayKmContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporatContractFixedDayKmContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
