import { Component, OnInit } from '@angular/core';
import {VechicleTransactionService} from "../../services/vechicle-transaction.service";
import {FormService} from "../../services/form.service";
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";
import {AssetService} from "../../services/asset.service";
import {VehicleCorporateContractService} from "../../services/vehicle-corporate-contract.service";
import {CorporateContractFieldsService} from "../../services/corporate-contract-fields.service";
import {CctransactionService} from "../../services/cctransaction.service";
import {Subscription} from "rxjs";
import {ComponetCommunicationService} from "../../services/componet-communication.service";
import * as _ from 'lodash'
import {ExcelService} from "../../services/excel.service";
import * as XLSX from "xlsx";

@Component({
  selector: 'app-corporate-contract1',
  templateUrl: './corporate-contract1.component.html',
  styleUrls: ['./corporate-contract1.component.scss']
})
export class CorporateContract1Component implements OnInit {
    monthList=["January","February","March","April","May","June","July","August"
        ,"September","October","November","December"]
    allVechicleTypeDropDown:any;
    selectedTransactionMonth:any;
    lesserOnSubmissionName:string="";
    companyAssetDropDown:any;
    vehicleTypeSelected:any;
    selectedVehicleItems:any;
    companySelected:any;
    corporateContractFields:any;
    formConfigData:any={};
    transactionDateDetails:any={}
    public vechicleDropdownList = [];
    public CCList:any = [];
    public vehicleSelectedDetails = {};
    public vechicleSelectedVehicleType = "";
    public vechicleTransactionSelected = "";
    public companyTransactionSelected= "";
    corporateContractType:any= "contract type 1"
    companyDropdownSettings = {
        singleSelection:false,
        idField: '_id',
        textField: 'companyName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
    public vechicleContract1dropdownSettings = {
        singleSelection:false,
        idField: '_id',
        textField: 'vehicleNumber',
        allowSearchFilter: true,
        closeDropDownOnSelection:true,

    };

  /*  selectMonth(month){
        console.log("*********************"+month)
        this.selectedTransactionMonth=month[0]

    }*/

    subscription: Subscription;
    constructor( public assetService:AssetService,
                 public vehicleCorporateContractService:VehicleCorporateContractService,
                 public corporateContractFieldsService:CorporateContractFieldsService,
                 public cctransactionService:CctransactionService,
                 public componetCommunicationService: ComponetCommunicationService,
                 public vechicleTypeServiceService:VechicleTypeServiceService,
                 public excelService:ExcelService){
            this.subscription = this.componetCommunicationService.getCCSelected().subscribe(ccType => {
            this.corporateContractType=ccType


        });
    }

    getIncialConfig(){
        this.getAllCompanyOfTypeCC1();
        this.getAllVechicleAsset();
        this.getCCIncialConfigData();
    }


    getAllCompanyOfTypeCC1(){
        this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
            .subscribe((companyList: any) => {
                this.companyAssetDropDown =_.uniqBy(companyList,function(companyList){ return companyList['companySelectedForDropDown']});
            })
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList
            })
    }
    getCorporateContractBasedOnrange(skip, limit) {
        this.vehicleCorporateContractService.getAllVehicleCorporateContract(skip, limit)
            .subscribe(corporateContractDetails => {
                this.companyAssetDropDown =corporateContractDetails
            });
    }

    onDeSelectVehicle(){
        this.vehicleSelectedDetails=""

    }
    onvehicleSelect(vehicleDetails){

        this.assetService.getAssetDetailsByMongodbId(vehicleDetails._id)
            .subscribe((vehicleDetails: any) => {
                console.log(vehicleDetails)
                this.vehicleSelectedDetails=vehicleDetails
                //remove
                this.vechicleSelectedVehicleType=vehicleDetails.vehicleType

            })
    }
    onCompanyDeSelect(companyDetails){
    this.getCCIncialConfigData();
    }

    onVehicleDeSelect(companyDetails){
        this.getCCIncialConfigData();
    }



    fetchCorporateConfigOnCompanySelected(companyDetails){
     this.companySelected=companyDetails
             this.fetchDBCCConfigDetailsOnSelection(companyDetails,this.vehicleSelectedDetails,this.corporateContractType)
    }



//fetch owned vehicle config
    fetchDBCCConfigDetailsOnSelection(companyDetails,vehicleDetails,ccType){
    console.log(vehicleDetails)
        if(vehicleDetails['rentalType']==="Owned"){
              this.fromOwnedConfig(vehicleDetails.vehicleType,ccType,companyDetails.companyName)
        }
         if(vehicleDetails['rentalType']==="Leased"){
                     this.fromLeasedConfig(vehicleDetails.vehicleType,ccType,companyDetails.companyName,vehicleDetails.lesserName)
                }

    }

     fromLeasedConfig(vehicleType,ccType,companyName,lesserName){
  this.vehicleCorporateContractService.getContractDetailsByLeasedType(
                       lesserName,ccType,vehicleType,companyName)
                                    .subscribe((companyConfigDetails: any) => {
                                         if(companyConfigDetails && companyConfigDetails[0]){
                                                this.createFromConfig(companyConfigDetails[0],
                                                this.corporateContractFieldsService.corporateContractTypes,
                                                vehicleType,lesserName)
                                         }
                                          else {
                                              alert("selected vehicle type   is not configured for lesser")
                                              }
                                    })

     }

    fromOwnedConfig(vehicleType,ccType,companyName){
     this.vehicleCorporateContractService.getContractDetailsByOwnedType(
                       vehicleType,ccType,companyName)
                                    .subscribe((companyConfigDetails: any) => {

                                         if(companyConfigDetails && companyConfigDetails[0]){
                                                this.createFromConfig(companyConfigDetails[0],
                                                this.corporateContractFieldsService.corporateContractTypes,
                                                vehicleType,"")
                                         }
                                          else {
                                              alert("selected vehicle type   is not configured")
                                              }
                                    })
    }



    createFromConfig(dbConfig,contractTypeFieldsList,vehicleType,lesserName){
                                for(var g=0;g<contractTypeFieldsList.length;g++){
                                    if(contractTypeFieldsList[g]['assetType']['typeId']==this.corporateContractType){
                                        let CCFieldsConfiguration = Object.keys(contractTypeFieldsList[g].configuration);
                                        let companyCCKeys = Object.keys(dbConfig);
                                        if(companyCCKeys){
                                            for(var k=0;k<companyCCKeys.length;k++){
                                                for(let p=0;p<CCFieldsConfiguration.length;p++){
                                                    if(companyCCKeys[k]== CCFieldsConfiguration[p]){
                                                        this.formConfigData
                                                            [contractTypeFieldsList[g].configuration[CCFieldsConfiguration[p]]['field']] =
                                                            dbConfig[companyCCKeys[k]]
                                                    }
                                                }
                                            }
                                            this.formConfigData['vehicleType']=vehicleType
                                        }

                                        console.log(this.corporateContractFieldsService.corporateContractTypes[g])
                                        this.fromConfigObjectForJSON(contractTypeFieldsList[g],dbConfig,lesserName)

                                    }

                                }
    }

     fromConfigObjectForJSON(configDetailsObj,companyCCDetails,lesserName){
                let configObj=  configDetailsObj.configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push(configObj[prop]);
                }
                if(formData){
                    this.lesserOnSubmissionName=lesserName
                    this.corporateContractFields=formData;
                }
            }

                           submitTransaction(){
                           console.log(this.companySelected)
                                           let obj={}
                                            obj['RatePerTrip']=this.formConfigData['RatePerTrip']
                                            obj['gps']=this.formConfigData['gps']
                                            obj['numberOfTrips']=this.formConfigData['numberOfTrips']
                                            obj['corporateContract']=this.corporateContractType
                                            obj['transactionDate']=this.transactionDateDetails.transactionDate
                                            obj['vehicleNumber']=this.selectedVehicleItems[0]['vehicleNumber']
                                            obj['vehicleType']=this.formConfigData['vehicleType']
                                            obj['ExtradutyRate']=this.formConfigData['ExtradutyRate']
                                            obj['companyTransactionSelected']=this.companySelected['companyName']
                                            if(this.lesserOnSubmissionName!==""){
                                            obj['lesserName']=this.lesserOnSubmissionName
                                            }
                                            console.log("************this.selecteobj33333333333333ctionMonth")
                                                                  console.log(obj)
                                            this.computeTransaction(obj)


        }




    computeTransaction(transactionFields){
        transactionFields['totalAmt']=((transactionFields['numberOfTrips']*transactionFields['RatePerTrip']))
        transactionFields['TDS']=(transactionFields['totalAmt']*1)/100
        transactionFields['payable']=transactionFields['totalAmt']+
            (transactionFields['ExtradutyRate']-transactionFields['TDS']-transactionFields['gps']);
        console.log("contractOwnedDetails?????11111111111???????")
        console.log(transactionFields)
        this.saveTransactionToDb(transactionFields)
    }


    saveTransactionToDb(transactionObj){
        this.cctransactionService.saveCCTransaction(transactionObj)
            .subscribe((data: any) => {
                this.corporateContractFields=[];
                this.formConfigData={};
                this.getCCTransaction()
            });
    }



   /*gtContractDetailsByccTypeLesserAndcompany(ccType,companyName,lesserName){

    }*/

    getCCIncialConfigData(){
        this.selectedVehicleItems=""
        this.companySelected=""
        this.corporateContractFields=[];
        this.companyTransactionSelected=""

    }



    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)
                this.allVechicleTypeDropDown=vechicleTypes
            })
    }


    getCCTransaction() {
        this.cctransactionService.getCCTransactionByType(this.corporateContractType)
            .subscribe(CCListDetails => {
                this.CCList =CCListDetails
            });
    }


    deleteCC1Trasnaction(ccDetails){
        this.cctransactionService.deleteCCTransactionByMongodbId(ccDetails._id)
            .subscribe((data: any) => {
                this.getCCTransaction()
            })
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
    this.deleteTransactionData=transactionData
    }

    data: any = [{
        SlNo: 0,
        Company:"",
        VehicleNumber: "",
        Mode:"",
        NumberOfDuty:0,
        ExtraDutyRate:0,
        GPS: 0,

    }];

    exportAsXLSX():void {
        this.excelService.exportAsExcelFile(this.data, 'cc1TemplateFile');
    }


    listData:any
    listIntermediateData:any
    tableDataObj:any=[];
    cc1ExcelDetails:any;
    uploadCc1ExcelDetails(event){
        let that=this
        let tableObjs;
        let input = event   .target;
        let reader = new FileReader();
        reader.onload = function(){
            let fileData = reader.result;
            let wb = XLSX.read(fileData, {type : 'binary'});
            wb.SheetNames.forEach(function(sheetName){
                let rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
                let tableObjs=JSON.stringify(rowObj);

                if (tableObjs == undefined || tableObjs== null){
                    return;
                }
                else{
                    console.log(JSON.stringify(rowObj))
                    that.listData=JSON.stringify(rowObj)
                    that.saveIntermediate(that.listData)
                }
            })
        };
        reader.readAsBinaryString(input.files[0])


    }


    saveIntermediate(listData){
        console.log("intermediate")
        this.listIntermediateData=listData
    }

    saveCC1DetaisToDB(){
        let listData=this.listIntermediateData
        var arrayListData = JSON.parse("[" + listData + "]");
        for(let p=0;p<arrayListData[0].length;p++){
            this.assetService.getAssetDetailsByVehicleId(arrayListData[0][p]['VehicleNumber'])
                        .subscribe((vehicleDetails: any) => {

                            if(vehicleDetails.length!==0){
                              console.log("saveCC1DetaisToDB-----------listData-------")
                                                         console.log(vehicleDetails)
                                                         console.log(vehicleDetails[0]['vehicleType'])
                             if(vehicleDetails[0]['rentalType']= "Owned"){
                             this.fromExcelOwnedConfig(vehicleDetails[0]['vehicleType'],
                             this.corporateContractType,
                             arrayListData[0][p]['Company'],arrayListData[0])
                              }
                             if(vehicleDetails[0]['rentalType']= "Leased"){
                             this.fromExcelLeasedConfig(vehicleDetails[0]['vehicleType'],
                             this.corporateContractType,
                             arrayListData[0][p]['Company'],
                             vehicleDetails[0].lesserName,arrayListData[0])
                              }
                                }
                                else{
                                console.log("vehicle not configured"+vehicleDetails.vehicleNumber)

                                }


                        })

        }
    }


     fromExcelLeasedConfig(vehicleType,ccType,companyName,lesserName,excelObj){
  this.vehicleCorporateContractService.getContractDetailsByLeasedType(
                       lesserName,ccType,vehicleType,companyName)
                                    .subscribe((companyConfigDetails: any) => {
                                         if(companyConfigDetails && companyConfigDetails[0]){
                                                console.log(companyConfigDetails)
                                                this.submitExcelTransaction(companyConfigDetails,excelObj,lesserName)
                                         }
                                          else {
                                              alert("selected vehicle type   is not configured for lesser")
                                              }
                                    })

     }

    fromExcelOwnedConfig(vehicleType,ccType,companyName,excelObj){
     this.vehicleCorporateContractService.getContractDetailsByOwnedType(
                       vehicleType,ccType,companyName)
                                    .subscribe((companyConfigDetails: any) => {

                                         if(companyConfigDetails && companyConfigDetails[0]){
                                              this.submitExcelTransaction(companyConfigDetails,excelObj,"")
                                         }
                                          else {
                                              alert("selected vehicle type   is not configured")
                                              }
                                    })
    }


                           submitExcelTransaction(configObj,excelObj,lesserName){
                           console.log(this.companySelected)
                                           let obj={}
                                            obj['RatePerTrip']=configObj['RatePerTrip']
                                            obj['gps']=excelObj['gps']
                                            obj['numberOfTrips']=excelObj['numberOfTrips']
                                            obj['corporateContract']=this.corporateContractType
                                            obj['transactionDate']=excelObj.transactionDate
                                            obj['vehicleNumber']=excelObj['vehicleNumber']
                                            obj['vehicleType']=excelObj['vehicleType']
                                            obj['ExtradutyRate']=excelObj['ExtradutyRate']
                                            if(lesserName!==""){
                                            obj['lesserName']=lesserName
                                            }
                                            obj['companyTransactionSelected']=excelObj['companyName']
                                            console.log("************this.selecteobj33333333333333ctionMonth")
                                                                  console.log(obj)
                                            this.computeTransaction(obj)


        }



    ngOnInit() {
        this.getAllVechicleType();
        this.getCCTransaction();

    }


}
