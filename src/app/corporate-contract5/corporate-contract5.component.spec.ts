import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateContract5Component } from './corporate-contract5.component';

describe('CorporateContract5Component', () => {
  let component: CorporateContract5Component;
  let fixture: ComponentFixture<CorporateContract5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateContract5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateContract5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
