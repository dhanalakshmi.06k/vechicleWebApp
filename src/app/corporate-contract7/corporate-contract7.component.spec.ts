import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateContract7Component } from './corporate-contract7.component';

describe('CorporateContract7Component', () => {
  let component: CorporateContract7Component;
  let fixture: ComponentFixture<CorporateContract7Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateContract7Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateContract7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
