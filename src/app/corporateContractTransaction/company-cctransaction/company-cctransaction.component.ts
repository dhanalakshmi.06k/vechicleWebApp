import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-company-cctransaction',
  templateUrl: './company-cctransaction.component.html',
  styleUrls: ['./company-cctransaction.component.scss']
})
export class CompanyCctransactionComponent implements OnInit {


   constructor(private router: Router) {
    }

    navigateCC1() {
        this.router.navigate(['cc1']);
    }

    navigateCC2() {

        this.router.navigate(['cc2']);
    }

    navigateCC3() {

        this.router.navigate(['cc3']);
    }
    navigateCC4() {

        this.router.navigate(['cc4']);
    }

    navigateCC5() {

        this.router.navigate(['cc5']);
    }
    navigateCC6() {
        this.router.navigate(['cc6']);
    }

    navigateCC7() {
        this.router.navigate(['cc7']);
    }

    navigateFixedDayKms(){
        this.router.navigate(['vehicleCorporatContractFixedDayKm']);
    }

  ngOnInit() {
  }

}
