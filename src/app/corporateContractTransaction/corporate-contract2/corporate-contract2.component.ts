import { Component, OnInit } from '@angular/core';
import * as XLSX from "xlsx";
import * as _ from 'lodash'
import {CctransactionService} from "../../../services/cctransaction.service";
import {AssetService} from "../../../services/asset.service";
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import {ExcelService} from "../../../services/excel.service";
@Component({
  selector: 'app-corporate-contract2',
  templateUrl: './corporate-contract2.component.html',
  styleUrls: ['./corporate-contract2.component.scss']
})
export class CorporateContract2Component implements OnInit {

  public CC2List:any=[];
    public vechicleDropdownList = [];
    companyAssetDropDown:any;
    formConfigData:any={};
    transactionDate:any={};
    public vechicleTransactionSelected = "";
    public selectedVehicleItems = "";
    public companySelected = "";
    companyTransactionSelected:any={}
    corporateContractFields:any;
    corporateContractType:any= "contract type 2";
    public vechicleSelectedDetails = {};
    public vechicleSelectedVehicleType = "";
    bulkUpload:any={}
    companyDropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'companySelectedForDropDown',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
     vechicledropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'vehicleNumber',
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    };
  constructor(public cctransactionService: CctransactionService,
              private assetService:AssetService,
              private vechicleTypeServiceService: VechicleTypeServiceService,
              private vehicleCorporateContractService:VehicleCorporateContractService,
              private corporateContractFieldsService:CorporateContractFieldsService,
              private excelService:ExcelService) { }


    getIncialConfig(){
        this.getAllCompanyOfTypeCC2();
        this.getAllVechicleAsset();
        this.getCCIncialConfigData();
    }

    getCCIncialConfigData(){
        this.selectedVehicleItems=""
        this.companySelected=""
        this.corporateContractFields=[];
        this.companyTransactionSelected=""
        this.vechicleTransactionSelected=""
        this.vechicleSelectedDetails=""
        this.vechicleSelectedVehicleType=""
    }


    onvehicleSelect(vehicleDetails){
        console.log("8328472384718vehicleDetails92347891237413")
        console.log(vehicleDetails._id)
        this.assetService.getAssetDetailsByMongodbId(vehicleDetails._id)
            .subscribe((vehicleDetails: any) => {
                console.log("******vechicleList*************");
                console.log(vehicleDetails);
                this.vechicleSelectedDetails=vehicleDetails
                this.vechicleSelectedVehicleType=vehicleDetails.vehicleType

            })
    }

    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                console.log("******vechicleList*************");
                console.log(vechicleList)
                this.vechicleDropdownList =vechicleList
            })
    }

    getAllCompanyOfTypeCC2(){
        this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
            .subscribe((companyList: any) => {
                console.log(companyList)
                this.companyAssetDropDown =_.uniqBy(companyList,function(companyList){ return companyList['companySelectedForDropDown']});
            })
    }
    onCompanySelect(companyDetails){
        this.companyTransactionSelected=companyDetails
        this.vehicleCorporateContractService.getVehicleCorporateContractByMongodbId(companyDetails['_id'])
            .subscribe((companyCC: any) => {

                console.log("this.onCompanySelect-=------------onCompanySelect--------")
                console.log(companyCC[0].companySelected._id)
                console.log(this.corporateContractType)
                console.log(this.vechicleSelectedVehicleType)
                this.vehicleCorporateContractService.getcompanyCCByMongodbIdAndCCTypeAndvehicleType( companyCC[0].companySelected._id ,this.corporateContractType
                    ,this.vechicleSelectedVehicleType)
                    .subscribe((companyCCDetails: any) => {
                        if(companyCCDetails && companyCCDetails[0]){
                            companyCCDetails=companyCCDetails[0];
                            for(var g=0;g<this.corporateContractFieldsService.corporateContractTypes.length;g++){
                                if(this.corporateContractFieldsService.corporateContractTypes[g]['assetType']['typeId']==
                                    companyCCDetails.corporateContract){
                                    let CCFieldsConfiguration = Object.keys(this.corporateContractFieldsService.corporateContractTypes[g].configuration);
                                    let companyCCKeys = Object.keys(companyCCDetails);
                                    let VehicleCCKeys = Object.keys(this.vechicleSelectedDetails);
                                    if(companyCCKeys){
                                        for(var k=0;k<companyCCKeys.length;k++){
                                            for(let p=0;p<CCFieldsConfiguration.length;p++){
                                                if(companyCCKeys[k]== CCFieldsConfiguration[p]){
                                                    this.formConfigData
                                                        [this.corporateContractFieldsService.corporateContractTypes[g].configuration[CCFieldsConfiguration[p]]['field']] =
                                                        companyCCDetails[companyCCKeys[k]]
                                                }
                                            }
                                        }

                                        this.formConfigData['vehicleType']=this.vechicleSelectedVehicleType;

                                    }
                                    console.log(this.corporateContractFieldsService.corporateContractTypes[g])
                                    this.fromConfigObjectForFrom(this.corporateContractFieldsService.corporateContractTypes[g],companyCCDetails)
                                }

                            }
                        }
                        else {
                            alert("selected vehicle type  is not configured")
                        }


                    });

            })



    }


    fromConfigObjectForFrom(configDetailsObj,companyCCDetails){
        let configObj=  configDetailsObj.configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for ( var prop of responseKeys) {
            formData.push(configObj[prop]);
        }
        if(formData){
            this.corporateContractFields=formData;
        }


    }


    submitTransaction(){

      let driverCost=0

        let obj={}
        obj['transactionDate']=this.bulkUpload.transactionDate
        // obj['transactionEndDate']=this.bulkUpload.transactionEndDate
        obj['companyTransactionSelected']=this.companyTransactionSelected['companySelectedForDropDown']
        obj['costPerkm']=this.formConfigData['costPerkm']
        obj['corporateContract']=this.corporateContractType
        if(this.formConfigData['fixedCostDoubleDriver']){
            driverCost=this.formConfigData['fixedCostDoubleDriver']
            obj['fixedCostDoubleDriver']=this.formConfigData['fixedCostDoubleDriver']
        }
        else{
            driverCost=0
            obj['fixedCostDoubleDriver']=0
        }

        if(this.formConfigData['fixedCostSingleDriver']){
            driverCost=this.formConfigData['fixedCostSingleDriver']
            obj['fixedCostSingleDriver']=this.formConfigData['fixedCostSingleDriver']
        }
        else{
            driverCost=0
            obj['fixedCostSingleDriver']=0
        }
        obj['fxdAndLesCost']=this.formConfigData['leasedAmount']+driverCost
        obj['leasedAmount']=this.formConfigData['leasedAmount']
        obj['halting']=this.formConfigData['halting']
        obj['tollFees']=this.formConfigData['tollFees']
        obj['runningCost']=this.formConfigData['totalKms']*this.formConfigData['costPerkm']
        obj['tollFees']=this.formConfigData['tollFees']
        obj['totalKms']=this.formConfigData['totalKms']
        obj['vehicleType']=this.formConfigData['vehicleType']
        obj['vehicleNumber']=this.vechicleSelectedDetails['vehicleNumber']
        obj['totalAmt']=(obj['fixedCostDoubleDriver']+obj['fixedCostSingleDriver']
        +obj['fxdAndLesCost']+this.formConfigData['halting']+this.formConfigData['tollFees']
        +obj['runningCost'])
        console.log("**************obj**** final======*************")
        console.log(obj)

        this.cctransactionService.saveCCTransaction(obj)
            .subscribe((data: any) => {
                this.getCCTransaction();
                this.getCCIncialConfigData();
            });

    }

    deleteCC1Trasnaction(ccDetails){
        console.log("ccDetails0000000000000werwerwer------------")
        console.log(ccDetails)
        this.cctransactionService.deleteCCTransactionByMongodbId(ccDetails._id)
            .subscribe((data: any) => {
                this.getCCTransaction()
            })
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }

    getCCTransaction() {
        this.cctransactionService.getCCTransactionByType(this.corporateContractType)
            .subscribe(CCListDetails => {
                this.CC2List =CCListDetails
            });
    }


    data: any = [{
        SlNo: 0,
        company: "",
        vehicleNumber: "",
        mode:"",
        totalKms:0,
        halting:0,
        tollFees:0,
        gps:0
    }];

    exportAsXLSX():void {
        this.excelService.exportAsExcelFile(this.data, 'driverTransaction');
    }


    listData:any
    listIntermediateData:any
    tableDataObj:any=[];
    cc1ExcelDetails:any;
    uploadCc1ExcelDetails(event){
        let that=this
        let tableObjs;
        let input = event   .target;
        let reader = new FileReader();
        reader.onload = function(){
            let fileData = reader.result;
            let wb = XLSX.read(fileData, {type : 'binary'});
            wb.SheetNames.forEach(function(sheetName){
                let rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
                let tableObjs=JSON.stringify(rowObj);

                if (tableObjs == undefined || tableObjs== null){
                    return;
                }
                else{
                    console.log(JSON.stringify(rowObj))
                    that.listData=JSON.stringify(rowObj)
                    that.saveIntermediate(that.listData)
                }
            })
        };
        reader.readAsBinaryString(input.files[0])


    }
    saveIntermediate(listData){
        console.log("intermediate")
        this.listIntermediateData=listData
    }

    saveCC2DetaisToDB(){
        var arrayListData = JSON.parse("[" + this.listIntermediateData + "]");
        console.log("arrayListData from db>>>>>>>>>>>>>")
        console.log(arrayListData)
        for(let p=0;p<arrayListData[0].length;p++){
            this.vehicleCorporateContractService.getcompanyCCByCompanyAndCCTypeAndVehicleType(
                arrayListData[0][p]['company'],this.corporateContractType
                ,arrayListData[0][p]['mode'])
                .subscribe((companyCCDetails: any) =>{
                    if(companyCCDetails[0]){
                        let driverCost=0
                        let obj={}
                        obj['transactionDate']=this.bulkUpload.transactionDate
                        // obj['transactionEndDate']=this.bulkUpload.transactionEndDate
                        obj['companyTransactionSelected']=arrayListData[0][p]['company']
                        obj['costPerkm']=companyCCDetails[0]['costPerkm']
                        obj['vehicleNumber']=arrayListData[0][p]['vehicleNumber']
                        obj['corporateContract']=this.corporateContractType
                        if(companyCCDetails[0]['fixedCostDoubleDriver']){
                            obj['fixedCostDoubleDriver']=companyCCDetails[0]['fixedCostDoubleDriver']
                            driverCost=companyCCDetails[0]['fixedCostDoubleDriver']

                        }
                        else{
                            obj['fixedCostDoubleDriver']=0
                            driverCost=0
                        }

                        if(companyCCDetails[0]['fixedCostSingleDriver']){
                            obj['fixedCostSingleDriver']=companyCCDetails[0]['fixedCostSingleDriver']
                            driverCost=companyCCDetails[0]['fixedCostSingleDriver']
                        }
                        else{
                            obj['fixedCostSingleDriver']=0
                            driverCost=0
                        }
                        obj['leasedAmount']=companyCCDetails[0]['leasedAmount']
                        obj['fxdAndLesCost']=driverCost+companyCCDetails[0]['leasedAmount']
                        obj['leasedAmount']=companyCCDetails[0]['leasedAmount']
                        obj['halting']=arrayListData[0][p]['halting']
                        /*obj['leasedAmount']=this.formConfigData['leasedAmount']*/
                        // obj['noOfVehicle']=this.formConfigData['noOfVehicle']
                        obj['tollFees']=arrayListData[0][p]['tollFees']
                        obj['runningCost']=arrayListData[0][p]['totalKms']*companyCCDetails[0]['costPerkm']
                        obj['tollFees']=arrayListData[0][p]['tollFees']
                        obj['totalKms']=arrayListData[0][p]['totalKms']
                        obj['vehicleType']=arrayListData[0][p]['mode']
                        console.log(companyCCDetails[0]['leasedAmount'])
                        obj['totalAmt']=(obj['fixedCostSingleDriver']+obj['fixedCostDoubleDriver']
                            +obj['fxdAndLesCost']+arrayListData[0][p]['halting']+arrayListData[0][p]['tollFees']
                            +obj['runningCost']+companyCCDetails[0]['leasedAmount'])
                        console.log("**************obj**** final======*************")
                        console.log(obj)
                        this.cctransactionService.saveCCTransaction(obj)
                            .subscribe((data: any) => {
                                this.getCCTransaction();
                                this.getCCIncialConfigData();
                            });

                    }

                    else{
                        alert(arrayListData[0][p]['company']+"is not configured of type"+arrayListData[0][p]['mode']);
                    }
                })

        }

    }

    ngOnInit() {
        this.getCCTransaction()
    }
}
