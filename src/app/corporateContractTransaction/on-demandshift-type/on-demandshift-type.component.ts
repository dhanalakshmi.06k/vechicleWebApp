import { Component, OnInit } from '@angular/core';
import {CctransactionService} from "../../../services/cctransaction.service";
import {AssetService} from "../../../services/asset.service";
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import * as _ from "lodash";
import {ShiftTypeServiceService} from "../../../services/shift-type-service.service";
import * as XLSX from "xlsx";
import {ExcelService} from "../../../services/excel.service";

@Component({
  selector: 'app-on-demandshift-type',
  templateUrl: './on-demandshift-type.component.html',
  styleUrls: ['./on-demandshift-type.component.scss']
})
export class OnDemandshiftTypeComponent implements OnInit {


    public CC5List: any = [];

    public vechicleTransactionSelected = "";
    public selectedVehicleItems = "";
    public companySelected = "";
    public bulkUpload:any={}
    companyAssetDropDown: any;
    corporateContractType: any = "Shift Type"
    vechicleDropdownList: any = [];
    corporateContractFields: any;
    transactionDate:any;
    companyTransactionSelected: any = {}
    formConfigData: any = {};
    companySelectedDetails = ""
    public vechicleSelectedVehicleType = "";
    public vechicleSelectedDetails = {};
    public shiftSelectedDetails;
    companyDropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'companySelectedForDropDown',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection: true
    }
    vechicledropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'vehicleNumber',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
    };
    shiftTypeDropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'ShiftTypeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
    shiftTypesDropDown:any;


    constructor(public cctransactionService: CctransactionService,
                private assetService: AssetService,
                public shiftTypeServiceService:ShiftTypeServiceService,
                private vechicleTypeServiceService: VechicleTypeServiceService,
                private vehicleCorporateContractService: VehicleCorporateContractService,
                private corporateContractFieldsService: CorporateContractFieldsService,
                private excelService:ExcelService) {
    }

    getIncialConfig() {
        this.getAllCompanyOfTypeCC4();
        this.getAllVechicleAsset();
        this.getCCIncialConfigData();
        this.getAllShiftType();
    }
    getCCIncialConfigData(){
        this.selectedVehicleItems=""
        this.companySelected=""
        this.corporateContractFields=[];
        this.companyTransactionSelected=""
        this.vechicleTransactionSelected=""
        this.vechicleSelectedDetails=""
        this.vechicleSelectedVehicleType=""
    }

    getAllCompanyOfTypeCC4() {
        this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
            .subscribe((companyList: any) => {
                this.companyAssetDropDown = _.uniqBy(companyList, function (companyList) {
                    return companyList['companySelectedForDropDown']
                });

                /*this.companyAssetDropDown =companyList*/
            })
    }

    getAllVechicleAsset() {
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList = vechicleList;

            })
    }

    getCCTransaction() {
        this.cctransactionService.getCCTransactionByType(this.corporateContractType)
            .subscribe(CCListDetails => {
                this.CC5List = CCListDetails
            });
    }


    onvehicleSelect(vehicleDetails) {
        console.log("8328472384718vehicleDetails92347891237413")
        console.log(vehicleDetails._id)
        this.assetService.getAssetDetailsByMongodbId(vehicleDetails._id)
            .subscribe((vehicleDetails: any) => {
                console.log("******vechicleList*************");
                console.log(vehicleDetails.vehicleType);
                this.vechicleSelectedDetails = vehicleDetails
                this.vechicleSelectedVehicleType = vehicleDetails.vehicleType

            })
    }

    onShiftSelect(shiftDetails){
       this.shiftSelectedDetails= shiftDetails;
    }


    onCompanySelect(companyDetails) {
        this.companyTransactionSelected = companyDetails
        this.vehicleCorporateContractService.getVehicleCorporateContractByMongodbId(companyDetails['_id'])
            .subscribe((companyCC: any) => {
                console.log("this.onCompanySelect-=------------++++++this.shiftSelectedDetails+++++++++++++--------")

                console.log(this.vechicleSelectedVehicleType)
                this.vehicleCorporateContractService.getcompanyCCByMongodbIdAndCCTypeAndvehicleTypeandShiftType(companyCC[0].companySelected._id, this.corporateContractType
                    , this.vechicleSelectedVehicleType,this.shiftSelectedDetails['ShiftTypeName'])
                    .subscribe((companyCCDetails: any) => {
                        if (companyCCDetails && companyCCDetails[0]) {
                            companyCCDetails = companyCCDetails[0];
                            for (var g = 0; g < this.corporateContractFieldsService.corporateContractTypes.length; g++) {
                                if (this.corporateContractFieldsService.corporateContractTypes[g]['assetType']['typeId'] ==
                                    companyCCDetails.corporateContract) {
                                    let CCFieldsConfiguration = Object.keys(this.corporateContractFieldsService.corporateContractTypes[g].configuration);
                                    let companyCCKeys = Object.keys(companyCCDetails);
                                    let VehicleCCKeys = Object.keys(this.vechicleSelectedDetails);
                                    if (companyCCKeys) {
                                        for (var k = 0; k < companyCCKeys.length; k++) {
                                            for (let p = 0; p < CCFieldsConfiguration.length; p++) {
                                                if (companyCCKeys[k] == CCFieldsConfiguration[p]) {
                                                    this.formConfigData
                                                        [this.corporateContractFieldsService.corporateContractTypes[g].configuration[CCFieldsConfiguration[p]]['field']] =
                                                        companyCCDetails[companyCCKeys[k]]
                                                }
                                            }
                                        }

                                        this.formConfigData['vehicleType'] = this.vechicleSelectedVehicleType;

                                    }
                                    console.log(this.corporateContractFieldsService.corporateContractTypes[g])
                                    this.fromConfigObjectForFrom(this.corporateContractFieldsService.corporateContractTypes[g], companyCCDetails)
                                }

                            }
                        }
                        else {
                            alert("selected vehicle type  is not configured")
                        }
                    });
            })


    }

    fromConfigObjectForFrom(configDetailsObj, companyCCDetails) {
        let configObj = configDetailsObj.configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for (var prop of responseKeys) {
            formData.push(configObj[prop]);
        }
        if (formData) {
            this.corporateContractFields = formData;
        }


    }

    deleteCC1Trasnaction(ccDetails) {
        console.log("ccDetails0000000000000werwerwer------------")
        console.log(ccDetails)
        this.cctransactionService.deleteCCTransactionByMongodbId(ccDetails._id)
            .subscribe((data: any) => {
                this.getCCTransaction()
            })
    }
     deleteTransactionData:any
     intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }


    submitTransaction() {
        var obj={}

        console.log("888888888888888this.formConfigData********************")
        console.log(this.formConfigData)
        obj['vehicleNumber'] = this.vechicleSelectedDetails
        obj['companyTransactionSelected'] = this.companyTransactionSelected
        obj['corporateContract'] = this.corporateContractType
        obj['vehicleType']=this.vechicleSelectedVehicleType
        obj['shiftType']=this.shiftSelectedDetails
        obj['vehicleTransactionType']=this.formConfigData['vehicleType']
        obj['fixedKmsDay']=this.formConfigData['fixedKmsDay']
        obj['ratePerKm']=this.formConfigData['ratePerKm']
        obj['ratePerShift']=this.formConfigData['ratePerShift']
        obj['minimumDaysFixed']=this.formConfigData['minimumDaysFixed']
        obj['route']=this.formConfigData['route']
        obj['transactionDate']=this.transactionDate
        obj['scheduledKmsDay']=this.formConfigData['scheduledKmsDay']
        obj['extraKms']=Math.abs(this.formConfigData['kmsPerDay']-this.formConfigData['fixedKmsDay'])


        obj['amountPerDay']=(this.formConfigData['ratePerShift']*2)+(Math.abs(this.formConfigData['kmsPerDay']-
            this.formConfigData['fixedKmsDay'])*this.formConfigData['ratePerKm'])

/*        if(this.formConfigData['numberOfDays']>this.formConfigData['minimumDaysFixed']){

            obj['extraDays']=Math.abs(this.formConfigData['minimumDaysFixed']-this.formConfigData['numberOfDays'])
        }*/
        obj['totalAmt']= this.formConfigData['minimumDaysFixed']*obj['amountPerDay']+(obj['extraDays']*obj['amountPerDay'])
        console.log("888888888888888this.totalllllllll********************")
        console.log(obj)
        this.cctransactionService.saveCCTransaction(obj)
            .subscribe((data: any) => {
                this.getCCTransaction()
            });

    }
    getAllShiftType(){
        this.shiftTypeServiceService.getShiftTypeCostDropDown()
            .subscribe((shiftTypes: any) => {
                console.log(shiftTypes)
                this.shiftTypesDropDown=shiftTypes
            })
    }

    tableDataObj:any=[];
    cc1ExcelDetails:any;
    listData:any
    listIntermediateData:any
    uploadShiftTypeExcelDetails(event){
        let that=this
        let tableObjs;
        let input = event   .target;
        let reader = new FileReader();
        reader.onload = function(){
            let fileData = reader.result;
            let wb = XLSX.read(fileData, {type : 'binary'});
            wb.SheetNames.forEach(function(sheetName){
                let rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
                let tableObjs=JSON.stringify(rowObj);

                if (tableObjs == undefined || tableObjs== null){
                    return;
                }
                else{
                    console.log(JSON.stringify(rowObj))
                    that.listData=JSON.stringify(rowObj)
                    that.saveIntermediate(that.listData)
                }
            })
        };
        reader.readAsBinaryString(input.files[0])


    }
    saveIntermediate(listData){
        console.log("intermediate")
        this.listIntermediateData=listData
    }

    saveShiftTypeDetaisToDB(){
        var arrayListData = JSON.parse("[" + this.listIntermediateData + "]");
        for(let p=0;p<arrayListData[0].length;p++){
            this.vehicleCorporateContractService.getcompanyCCByCompanyAndCCTypeAndVehicleTypeAndShiftType(
                arrayListData[0][p]['company'],this.corporateContractType
                ,arrayListData[0][p]['mode'],arrayListData[0][p]['shiftType'])
                .subscribe((companyCCDetails: any) =>{
                    console.log("companyCCDetails------------------")
                    console.log(companyCCDetails[0])
                    if(companyCCDetails[0]){
                        let obj={}
                        obj['vehicleType']=arrayListData[0][p]['mode']
                        obj['vehicleNumber'] = arrayListData[0][p]['vehicleNumber']
                        obj['companyTransactionSelected']=arrayListData[0][p]['company']
                        obj['corporateContract'] = this.corporateContractType
                        obj['shiftType']=arrayListData[0][p]['shiftType']
                        obj['scheduledKmsDay']=arrayListData[0][p]['scheduledKmsDay']
                        obj['fixedKmsDay']=companyCCDetails[0]['fixedKmsDay']
                        obj['ratePerKm']=companyCCDetails[0]['ratePerKm']
                        obj['ratePerShift']=companyCCDetails[0]['ratePerShift']
                        obj['minimumDaysFixed']=companyCCDetails[0]['minimumDaysFixed']
                        obj['route']=arrayListData[0][p]['route']
                        obj['transactionStartDate']=this.bulkUpload.transactionStartDate
                        obj['transactionEndDate']=this.bulkUpload.transactionEndDate

                        obj['scheduledKmsDay']=arrayListData[0][p]['scheduledKmsDay']

                        obj['extraKms']=Math.abs(arrayListData[0][p]['scheduledKmsDay']-companyCCDetails[0]['fixedKmsDay'])
                        obj['amountPerDay']=(companyCCDetails[0]['ratePerShift'])+(obj['extraKms']*companyCCDetails[0]['ratePerKm'])

                        if(arrayListData[0][p]['numberOfDays']!==0 && (companyCCDetails[0]['fixedKmsDay']>arrayListData[0][p]['numberOfDays'])){
                            obj['extraDays']=Math.abs(companyCCDetails[0]['fixedKmsDay']-arrayListData[0][p]['numberOfDays'])
                        }
                        obj['totalAmt']= companyCCDetails[0]['minimumDaysFixed']*obj['amountPerDay']+(obj['extraDays']*obj['amountPerDay'])
                        console.log("888888888888888this.totalllllllll********************")
                        console.log(obj)
                        this.cctransactionService.saveCCTransaction(obj)
                            .subscribe((data: any) => {
                                this.getCCTransaction()
                            });
                    }

                    else{
                        alert(arrayListData[0][p]['company']+"is not configured of type"+arrayListData[0][p]['mode']);
                    }
                })

        }

    }

    data: any = [{
        SlNo: 0,
        company:"",
        mode:" ",
        route: "",
        vehicleNumber: "",
        shiftType:"",
        scheduledKmsDay:0,
        amountPerDay:0,
        numberOfDays:0,
        splitTrip:0

    }];


    exportAsXLSX():void {
        this.excelService.exportAsExcelFile(this.data, 'shiftTypeSample');
    }

    ngOnInit() {
        this.getCCTransaction();
    }
}
