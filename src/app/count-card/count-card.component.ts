import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-count-card',
    templateUrl: './count-card.component.html',
    styleUrls: ['./count-card.component.scss']
})
export class CountCardComponent implements OnInit {
    @Input() dashData: any;
    @Output() filterAssetType: EventEmitter<string> = new EventEmitter();
    icon: string;
    embossedFlag: boolean = false;
    showCloseBtn: boolean = false;
    public car = 'fa-user';

    constructor() {
    }

    getBackgroundColorLeftCard(type) {
        switch (type) {
            case 'Vehicle':
                return '#7986CB';
            case 'Lesser':
                return '#4DB6AC';
            case 'Company':
                return '#81C784';
            case 'Driver':
                return '#d27f72c2';
        }
    }

    getBackgroundColorRightCard(type) {
        switch (type) {
            case 'Vehicle':
                return '#5C6BC0';
            case 'Lesser':
                return '#26A69A';
            case 'Company':
                return '#66BB6A';
            case 'Driver':
                return '#42A5F5';

        }
    }

    getIcon(type) {
        switch (type) {
            case 'Vehicle':
                return '../../assets/asset-icons/car.png';
            case 'Lesser':
                return '../../assets/asset-icons/conference-background-selected.png';
            case 'Company':
                return '../../assets/asset-icons/organization.png';
            case 'Driver':
                return '../../assets/asset-icons/driver.png';
        }
    }

    ngOnInit() {
        this.icon = this.getIcon(this.dashData.type);
    }

    filterAssetsOnType(assetType) {
        console.log("removing sfsfsf", assetType);
        this.embossedFlag = !this.embossedFlag;
        this.filterAssetType.emit(assetType);
    }

}
