import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormService} from "../../../../services/form.service";
import {AssetService} from "../../../../services/asset.service";
import {AssetCommunicationService} from "../../../../services/asset-communication.service";

@Component({
  selector: 'app-driver-asset-from',
  templateUrl: './driver-asset-from.component.html',
  styleUrls: ['./driver-asset-from.component.scss']
})
export class DriverAssetFromComponent implements OnInit {


    @Input() assetId: any;
    @Input() isEditMode: any;
    @Input() driverEditId: any;
    @Output() assetAdded: EventEmitter<any> = new EventEmitter();
    driverAssetDetails:any= {
        status:"Active",
        state:"Karnataka"
    };
    isEdit:any = false;
    mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$";

    constructor(private formService:FormService,
              private assetService:AssetService,
              private assetCommunicationService:AssetCommunicationService) { }
    @Output() savedAssetCard: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
      this. driverAssetDetails= {
          status:"Active",
          state:"Karnataka"
      }
  }

    submitConfigDetails() {
        this.driverAssetDetails.assetType = "Driver";
        this.assetService.saveAssetDetails( this.driverAssetDetails)
            .subscribe((savedAssetValue: any) => {
                this.assetAdded.emit(savedAssetValue);
                this. driverAssetDetails= {
                    status:"Active",
                    state:"Karnataka"
                }
            })

    }
    ngOnChanges() {
        if(!this.isEditMode){
            this. driverAssetDetails= {
                status:"Active",
                state:"Karnataka"
            }
        }
        else{
            this.assetService.getAssetDetailsByMongodbId(this.driverEditId)
                .subscribe((assetSpeicificDetails: any) => {
                    this.assetAdded.emit(assetSpeicificDetails);
                    this.driverAssetDetails=assetSpeicificDetails;
                })
        }

    }


    updateConfigDetails(){
        console.log("************this.companyAssetDetails**************")
        console.log(this.driverAssetDetails)
        this.assetService.updateAssetsById(this.driverAssetDetails._id,this.driverAssetDetails)
            .subscribe((updatedDetails: any) => {
                this.assetAdded.emit(updatedDetails);
            })
    }
    deleteAssetById(){
        this.assetService.deleteAssetDetailsByMongodbId(this.assetId)
            .subscribe((deleteAssetDetails: any) => {
                console.log("done ------assetSpeicificDetails---")
                console.log(deleteAssetDetails)
                this.assetCommunicationService.setAssetRefresh(deleteAssetDetails)
            })
    }


}
