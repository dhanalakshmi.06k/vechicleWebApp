import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LesserAssetFromComponent } from './lesser-asset-from.component';

describe('LesserAssetFromComponent', () => {
  let component: LesserAssetFromComponent;
  let fixture: ComponentFixture<LesserAssetFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LesserAssetFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LesserAssetFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
