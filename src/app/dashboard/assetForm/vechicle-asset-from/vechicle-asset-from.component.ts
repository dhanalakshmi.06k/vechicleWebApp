import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AssetService} from "../../../../services/asset.service";
import {VechicleTypeServiceService} from "../../../../services/vechicle-type-service.service";
import {AssetCommunicationService} from "../../../../services/asset-communication.service";
import {FuelServiceService} from "../../../../services/fuel-service.service";

@Component({
  selector: 'app-vechicle-asset-from',
  templateUrl: './vechicle-asset-from.component.html',
  styleUrls: ['./vechicle-asset-from.component.scss']
})
export class VechicleAssetFromComponent implements OnInit {

@Input() isEditMode: any;
@Input() vehicleEditId: any;
    @Input() assetId: any;
    @Input() driverEditId: any;
    @Output() assetAdded: EventEmitter<any> = new EventEmitter();
  constructor( private assetService:AssetService,
               private vechicleTypeServiceService:VechicleTypeServiceService,
               private assetCommunicationService:AssetCommunicationService,
               private fuelServiceService:FuelServiceService) { }
    isEdit:any = false;
    selectedVehicleItems:any;

    selectedSaveLesserItems:any;
    vechicleTypesDropDown:any;
    fuleTypesDropDown:any;
    lesserTypesDropDown:any;
    companyTypesDropDown:any;
    isLesserActive:any=false;


    vechicleAssetDetails:any={
        status:"Active",
        rentalType:"Owned",


    }


    showLesserField(state){
        if(state==='Leased'){
            this.isLesserActive=true
        }
        else{
            this.isLesserActive=false
        }

    }

    public vehicledropdownSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'vehicleTypeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    };



    onLesserSelect(lesserId){

        this.selectedSaveLesserItems=lesserId
    }
    onLesserDeSelect(lesserId){

        this.selectedSaveLesserItems=lesserId
    }

    public lesserdropdownSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'lesserName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    };

    submitConfigDetails() {
        console.log("before save66666666666666666")
        console.log(this.vechicleAssetDetails)
        this.vechicleAssetDetails.assetType = "Vehicle";
        this.vechicleAssetDetails.lesserDetails = this.selectedSaveLesserItems
        // this.vechicleAssetDetails.vechicleType = this.selectedSaveVehicleItems
        this.assetService.saveAssetDetails( this.vechicleAssetDetails )
            .subscribe((savedAssetValue: any) => {
                this.assetAdded.emit(savedAssetValue);
                this.vechicleAssetDetails={
                    status:"Active",
                    rentalType:"Owned",

                }

            })

    }
    ngOnChanges() {
        if(!this.isEditMode){
            this.vechicleAssetDetails={
                status:"Active",
                rentalType:"Owned",


            }
        }else{
            this. getAllVechicleType()
            this. getAllLessers()
            this.assetService.getAssetDetailsByMongodbId(this.vehicleEditId)
                .subscribe((assetSpeicificDetails: any) => {
                    /*  this.selectedVehicleItems=assetSpeicificDetails.vechicleType;*/
                    this.vechicleAssetDetails=assetSpeicificDetails;
                    console.log("done ------assetSpeicificDetails---")
                    console.log(assetSpeicificDetails)
                    /* if(assetSpeicificDetails&&assetSpeicificDetails.lesserDetails&&assetSpeicificDetails.lesserDetailslesserName){
                         this.selectedLesserItems=assetSpeicificDetails.lesserDetails.lesserName;
                     }*/

                })
        }

    }
    updateConfigDetails(){
        this.assetService.updateAssetsById(this.vechicleAssetDetails._id,this.vechicleAssetDetails)
            .subscribe((updatedDetails: any) => {
                this.assetAdded.emit(updatedDetails);
            })
    }

    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
                console.log(vechicleTypes)
                this.vechicleTypesDropDown=vechicleTypes
            })
    }
    getAllLessers(){
        this.assetService.getAssetsByTypeName('Lesser')
            .subscribe((lesser: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
                console.log(lesser)
                this.lesserTypesDropDown=lesser
            })
    }

    deleteAssetById(){
        this.assetService.deleteAssetDetailsByMongodbId(this.assetId)
            .subscribe((deleteAssetDetails: any) => {
                console.log("done ------assetSpeicificDetails---")
                console.log(deleteAssetDetails)
                this.assetCommunicationService.setAssetRefresh(deleteAssetDetails)
            })
    }
    ngOnInit() {
        console.log("*******sdfdfsfskldjf***********")
        this.vechicleAssetDetails['dateOfJoining']=new Date()
        this.isLesserActive=false
        this. getAllVechicleType()
        this. getAllFuelType()
        this. getAllLessers();
        this.getAllCompany()
       this.vechicleAssetDetails={
            status:"Active",
            rentalType:"Owned",

        }
    }
    getAllFuelType(){
        this.fuelServiceService.getFuelTypeForDropDown()
            .subscribe((fuelTypes: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
                console.log(fuelTypes)
                this.fuleTypesDropDown=fuelTypes
            })
    }

    getAllCompany(){
        this.assetService.getAssetsByTypeName('Company')
            .subscribe((company: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
                console.log(company)
                this.companyTypesDropDown=company
            })
    }


}
