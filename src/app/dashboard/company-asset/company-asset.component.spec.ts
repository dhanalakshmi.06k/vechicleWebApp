import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyAssetComponent } from './company-asset.component';

describe('CompanyAssetComponent', () => {
  let component: CompanyAssetComponent;
  let fixture: ComponentFixture<CompanyAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
