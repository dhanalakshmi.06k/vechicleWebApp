import { Component, OnInit } from '@angular/core';
import {AssetService} from "../../../services/asset.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import {ExcelService} from "../../../services/excel.service";
@Component({
  selector: 'app-company-asset',
  templateUrl: './company-asset.component.html',
  styleUrls: ['./company-asset.component.scss']
})
export class CompanyAssetComponent implements OnInit {
isEdit:boolean=false;
assetType : string = "Company";
companyAssetList:any=[];
companyDetails:any={}
    assetListForReport:any=[];
companyEditDetails:any={}
    public pagination: any;
    public skip: number;
    public limit: number;
  constructor(public assetService:AssetService,private excelService:ExcelService) {
      this.pagination = {
          'currentPage': 1,
          'totalPageCount': 0,
          'recordsPerPage': 10
      };
  }

  ngOnInit() {
      this.getAssetsCount()
      this.getAllAssets(0,10)
  }
    assetcompanyAdded(){
        this.getAllAssets(0,10)
    }

    getAssetsCount(){
        this.assetService.getAssetPaginationCountByAssetType( this.assetType)
            .subscribe((countData: any) => {
                console.log("count===============")
                console.log(countData)
                this.pagination.totalPageCount=countData.count
            })
    }

    getAllAssets(skip,limit){
        this.assetService.getAssetByRange( this.assetType,skip,limit )
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.companyAssetList=assetList
            })
    }



    intermerdiateDeleteTranasaction(companyDetail){
        this.isEdit=true
       this.companyDetails=companyDetail
    }
    deletecompanyDetails(companyDetail){
        this.assetService.deleteAssetDetailsByMongodbId(companyDetail._id)
            .subscribe((assetList: any) => {
                console.log(assetList)
                // this.deleteAssetCache(companyDetail)
                this.getAllAssets(0,10)
            })
    }

    deleteAssetCache(companyDetail){
        for(let j=0;j<this.companyAssetList.length;j++){
            console.log("7897897897")

            if(this.companyAssetList[j]._id===companyDetail['_id']){
                console.log(this.companyAssetList[j]._id)
                console.log(companyDetail['_id'])
                this.companyAssetList.splice(j,1);
            }
        }
    }
    editIntermediatecompany(companyDetail){
       this.isEdit=true
       this.companyEditDetails =companyDetail
    }


    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getAllAssets(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getAllAssets(this.skip, this.limit);
        }
    }

    addAsset(){
       this.isEdit=false
    }


    downLoadPdf():void {
        this.assetService.getAllAssetsForReport( this.assetType)
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.assetListForReport=assetList.reverse();
                this.excelService.exportAsExcelFile(this.assetListForReport, 'CompanyAsset');
            })

    }

  /*  columns = [];
    rows = [];
    fromReportData(){
        if(this.assetListForReport.length>0){
            let objectKeys=Object.keys(this.assetListForReport[0])
            for(let u=0;u<objectKeys.length;u++){
                let titleObj={}
                titleObj['title']=objectKeys[u].toUpperCase()
                titleObj['dataKey']=objectKeys[u]
                this.columns.push(titleObj)
            }
            console.log("8888888this.columns")
            console.log(this.columns)
            for(let j=0;j< this.assetListForReport.length;j++){
                console.log(this.assetListForReport[j])
                this.rows.push(this.assetListForReport[j])
            }
        }

    }


    downLoadPdf(){
        this.getAllAssetsForReport()
        let doc = new jsPDF();
        doc.autoTable(this.columns, this.rows,  {addPageContent: function(data) {
                doc.text("", 40, doc.internal.pageSize.height - 10);
            }});
        doc.save('LesserAsset.pdf')
    }

    getAllAssetsForReport(){
        this.assetService.getAllAssetsForReport( this.assetType)
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.assetListForReport=assetList.reverse();
                this.fromReportData()
            })
    }*/
}