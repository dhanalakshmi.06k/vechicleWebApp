import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LesserHeaderComponent } from './lesser-header.component';

describe('LesserHeaderComponent', () => {
  let component: LesserHeaderComponent;
  let fixture: ComponentFixture<LesserHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LesserHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LesserHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
