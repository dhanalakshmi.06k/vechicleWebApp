import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-lesser-header',
  templateUrl: './lesser-header.component.html',
  styleUrls: ['./lesser-header.component.scss']
})
export class LesserHeaderComponent implements OnInit {

    constructor(public router:Router) { }

    ngOnInit() {
    }

    navigateLesserAssociationOnDemand() {
        this.router.navigate(['lesserOnDemandAssociation']);
    }
    navigateLesserAssociationShiftType() {
        this.router.navigate(['lesserShiftTypeAssociation']);
    }
    navigateLesserAssociationCommonType() {
        this.router.navigate(['lesserCommonTypeAssociation']);
    }
    navigateLesserAssociationcc2Type() {
        this.router.navigate(['lessercc2TypeAssociation']);
    }
    navigateLesserAssociationcc1Type() {
        this.router.navigate(['lessercc1TypeAssociation']);
    }

}
