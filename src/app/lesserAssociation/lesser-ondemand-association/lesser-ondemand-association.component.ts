import { Component, OnInit } from '@angular/core';
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import {AssetService} from "../../../services/asset.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {Router} from "@angular/router";
import {LesserAssociationService} from "../../../services/lesserAssociation/lesser-association.service";
import * as _ from 'lodash'

@Component({
  selector: 'app-lesser-ondemand-association',
  templateUrl: './lesser-ondemand-association.component.html',
  styleUrls: ['./lesser-ondemand-association.component.scss']
})
export class LesserOndemandAssociationComponent implements OnInit {
    companyAssetDropDown:any=[]
    allVechicleTypeDropDown:any;
    formConfigData:any={};
    term:any;
    vechicleDropdownList:any;
    lesserDropdownList:any;
    corporateContractType="OnDemand";
    corporateContractFields:any=[];
    corporateContractDetailsList:any=[];
    lesserAssetDropDown:any;
    lesserSelected:any;
    vehicleTypeSelected:any;
    public pagination: any;
    isEdit=false
    companySelected:any;
    lesserDropdownSettings:any = {}
    vehicleTypeDropdownSettings:any = {}
    companyDropdownSettings = {
              singleSelection:false,
              idField: '_id',
              textField: 'companyName',
              selectAllText: 'Select All',
              unSelectAllText: 'UnSelect All',
              allowSearchFilter: true,
              closeDropDownOnSelection:true
          }

    constructor(public vechicleTypeServiceService:VechicleTypeServiceService,
                public corporateContractFieldsService:CorporateContractFieldsService,
                public assetService:AssetService,
                public vehicleCorporateContractService:VehicleCorporateContractService,
                private router: Router,
                private lesserAssociationService:LesserAssociationService) {
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
        this.corporateContractType="OnDemand"
        this.lesserDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'lesserName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.vehicleTypeDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'vehicleTypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }

    }

    onCompanySelectSelect(item: any) {

        this.companySelected=item;
        console.log(this.companySelected);

    }

    onSelectAll (items: any) {
        console.log(items);
    }
    onItemDeSelect(items:any){
        console.log(items)
    }
    submitVehicleCorporateContractDetails(){
        console.log(this.lesserSelected)
          this.formConfigData['vehicleType']=this.vehicleTypeSelected[0]['vehicleTypeName']
            this.formConfigData['lesserName']=this.lesserSelected[0]['lesserName']
            this.formConfigData['companyName']=this.companySelected[0]['companyName']
            this.formConfigData['corporateContract']= this.corporateContractType

            this.vehicleCorporateContractService.checkCCExitsBasedOnVehicleTypeAndName(
                     this.formConfigData['companyName'],this.corporateContractType,this.formConfigData['vehicleType'])
                     .subscribe((existResponse: any) => {
                         if(existResponse.length==0){
                             this.vehicleCorporateContractService.saveVehicleCorporateContract( this.formConfigData )
                                 .subscribe((ccCount: any) => {
                                     this.getCorporateContractTotalCountForPagination();
                                     this.getCorporateContractByType(this.corporateContractType)
                                 });
                         }
                         else{
                             alert(this.lesserSelected[0].lesserName +" is already configured for type" + this.formConfigData['vehicleTypeSelected']['vehicleTypeName'] )
                         }

                     })

        this.lesserAssociationService.saveLesserAssociation( this.formConfigData )
            .subscribe((ccCount: any) => {

                this.getCorporateContractTotalCountForPagination();
                this.getCorporateContractByType(this.corporateContractType)
            });

    }

    editCorporateContract(corporateContractDetails){
        console.log( corporateContractDetails)
        this.isEdit=true
        this.formConfigData['agreementStartDate']=corporateContractDetails['agreementStartDate']
        this.formConfigData['agreementEndDate']=corporateContractDetails['agreementEndDate']
        this.formConfigData=corporateContractDetails
        console.log("**************formConfigData*****ccCount************");
        console.log( this.formConfigData)
        console.log( this.corporateContractFields)
        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }

        }

    }

    getFromSetUpDetails(){
        this.vehicleTypeSelected=""
        this.lesserSelected ={};
        this.vehicleTypeSelected={}
        this.formConfigData={};
    }
    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)
                this.allVechicleTypeDropDown=vechicleTypes
            })
    }



    companyOnDemandAssoication(){
        this.router.navigate(['companyOnDemandAssoication']);
    }
    populateCorporateContractType(){
        this.isEdit=false
        this.getAllVechicleType()
        this.getAllLesserAsset();

        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }
            else{
                this.corporateContractFields=[]
            }
        }

    }


    updateVehicleCorporateContractDetails(){
        console.log("**************formConfigData*****************");
        console.log( this.formConfigData)
        this.lesserAssociationService.updatetLesserAssociationById(this.formConfigData['_id'],this.formConfigData)
            .subscribe(expenseData => {
                console.log("**************vehicleCorporateContractService updates*****************");

            });
    }

    deleteCCDetails(CCDetails){
        this.lesserAssociationService.deletetLesserAssociationByMongodbId(CCDetails['_id'])
            .subscribe(expenseData => {
                this.getCorporateContractTotalCountForPagination();
                this.getCorporateContractByType(this.corporateContractType)
            });
    }

    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }

    getCorporateContractTotalCountForPagination() {
        this.vehicleCorporateContractService.getVehicleCorporateContractCount()
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }
    getCorporateContractBasedOnrange(skip, limit) {
        this.vehicleCorporateContractService.getAllVehicleCorporateContractList()
            .subscribe((corporateContractDetails:any) => {
                console.log("----------typeof corporateContractDetails")
                console.log( corporateContractDetails)
                console.log(typeof corporateContractDetails)
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList
            })
    }



    getAllLesserAsset(){
        this.assetService.getAssetlistSpecificList("Lesser")
            .subscribe((companyList: any) => {
                this.lesserAssetDropDown =companyList
            })
    }

     getAllCompanyAsset(){
            this.assetService.getAssetlistSpecificList("Company")
                .subscribe((companyList: any) => {
                    this.companyAssetDropDown =companyList
                })
        }

    getCorporateContractByType(ccType) {
        this.lesserAssociationService.getLesserAssociationByCCtype(ccType)
            .subscribe((corporateContractDetails:any) => {
                console.log("----------typeof corporateContractDetails")
                console.log( corporateContractDetails)
                console.log(typeof corporateContractDetails)
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }



    ngOnInit() {
        this.getAllCompanyAsset()
        this.getCorporateContractByType(this.corporateContractType)
        this.getCorporateContractTotalCountForPagination();
    }



}
