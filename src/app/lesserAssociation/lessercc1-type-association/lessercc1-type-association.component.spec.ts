import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Lessercc1TypeAssociationComponent } from './lessercc1-type-association.component';

describe('Lessercc1TypeAssociationComponent', () => {
  let component: Lessercc1TypeAssociationComponent;
  let fixture: ComponentFixture<Lessercc1TypeAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Lessercc1TypeAssociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lessercc1TypeAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
