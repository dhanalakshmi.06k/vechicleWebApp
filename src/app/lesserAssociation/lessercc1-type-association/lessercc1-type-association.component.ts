import { Component, OnInit } from '@angular/core';
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import {AssetService} from "../../../services/asset.service";

import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {Router} from "@angular/router";
import * as _ from 'lodash'
import {LesserAssociationService} from "../../../services/lesserAssociation/lesser-association.service";

@Component({
  selector: 'app-lessercc1-type-association',
  templateUrl: './lessercc1-type-association.component.html',
  styleUrls: ['./lessercc1-type-association.component.scss']
})
export class Lessercc1TypeAssociationComponent implements OnInit {

  allVechicleTypeDropDown:any;
  companyAssetDropDown:any;
  formConfigData:any={};
  term:any;
  vechicleDropdownList:any;
  lesserDropdownList:any;
  corporateContractType="contract type 1";
  corporateContractFields:any=[];
  corporateContractDetailsList:any=[];
  currentCompanySelected:string;
  lesserAssetDropDown:any;
  lesserSelected:any;
  vehicleTypeSelected:any;
  public pagination: any;
  isEdit=false
  lesserDropdownSettings:any = {}
  vehicleTypeDropdownSettings:any = {}
   companyDropdownSettings = {
          singleSelection:false,
          idField: '_id',
          textField: 'companyName',
          selectAllText: 'Select All',
          unSelectAllText: 'UnSelect All',
          allowSearchFilter: true,
          closeDropDownOnSelection:true
      }


  constructor(public vechicleTypeServiceService:VechicleTypeServiceService,
              public corporateContractFieldsService:CorporateContractFieldsService,
              public assetService:AssetService,
              public vehicleCorporateContractService:VehicleCorporateContractService,
              private router: Router,
              private lesserAssociationService:LesserAssociationService) {
    this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
    this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
    this.pagination = {
      'currentPage': 1,
      'totalPageCount': 0,
      'recordsPerPage': 12
    };
    this.corporateContractType="contract type 1";
    this.lesserDropdownSettings = {
      singleSelection: true,
      idField: '_id',
      textField: 'lesserName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true,
      closeDropDownOnSelection:true
    }
    this.vehicleTypeDropdownSettings = {
      singleSelection: true,
      idField: '_id',
      textField: 'vehicleTypeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 1,
      allowSearchFilter: true,
      closeDropDownOnSelection:true
    }

  }

  onCompanySelectSelect(item: any) {

    this.currentCompanySelected=item;
    console.log(this.currentCompanySelected);

  }

  onSelectAll (items: any) {
    console.log(items);
  }
  onItemDeSelect(items:any){
    console.log(items)
  }
  submitVehicleCorporateContractDetails(){
    this.formConfigData['vehicleType']=this.vehicleTypeSelected[0]['vehicleTypeName']
    this.formConfigData['lesserName']=this.lesserSelected[0]['lesserName']
    this.formConfigData['companyName']=this.companyAssetDropDown[0]['companyName']
    this.formConfigData['corporateContract']= this.corporateContractType

     this.vehicleCorporateContractService.checkCCExitsBasedOnVehicleTypeAndName(
         this.formConfigData['companyName'],this.corporateContractType,this.formConfigData['vehicleType'])
         .subscribe((existResponse: any) => {
             if(existResponse.length==0){
                 this.vehicleCorporateContractService.saveVehicleCorporateContract( this.formConfigData )
                     .subscribe((ccCount: any) => {
                         this.getCorporateContractTotalCountForPagination();
                         this.getCorporateContractByType(this.corporateContractType)
                     });
             }
             else{
                 alert(this.lesserSelected[0].lesserName +" is already configured for type" + this.formConfigData['vehicleTypeSelected']['vehicleTypeName'] )
             }

         })

    this.lesserAssociationService.saveLesserAssociation( this.formConfigData )
        .subscribe((ccCount: any) => {

          this.getCorporateContractTotalCountForPagination();
          this.getCorporateContractByType(this.corporateContractType)
        });

  }

  editCorporateContract(corporateContractDetails){
    this.isEdit=true
    this.formConfigData['agreementStartDate']=corporateContractDetails['agreementStartDate']
    this.formConfigData['agreementEndDate']=corporateContractDetails['agreementEndDate']
    this.formConfigData=corporateContractDetails

    for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
      if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
          this.corporateContractType){
        let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for ( var prop of responseKeys) {
          formData.push( configObj[prop]);
        }
        this.corporateContractFields=formData
        break
      }

    }

  }

  getFromSetUpDetails(){
    this.vehicleTypeSelected=""
    this.lesserSelected ={};
    this.vehicleTypeSelected={}
    this.formConfigData={};
  }
  getAllVechicleType(){
    this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
        .subscribe((vechicleTypes: any) => {
          console.log(vechicleTypes)
          this.allVechicleTypeDropDown=vechicleTypes
        })
  }



  companyOnDemandAssoication(){
    this.router.navigate(['companyOnDemandAssoication']);
  }
  populateCorporateContractType(){
    this.isEdit=false
    for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
      if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
          this.corporateContractType){
        let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for ( var prop of responseKeys) {
          formData.push( configObj[prop]);
        }
        this.corporateContractFields=formData
        break
      }
      else{
        this.corporateContractFields=[]
      }
    }

  }


  updateVehicleCorporateContractDetails(){
    console.log("**************formConfigData*****************");
    console.log( this.formConfigData)
    this.lesserAssociationService.updatetLesserAssociationById(this.formConfigData['_id'],this.formConfigData)
        .subscribe(expenseData => {
          console.log("**************vehicleCorporateContractService updates*****************");

        });
  }

  deleteCCDetails(CCDetails){
    this.lesserAssociationService.deletetLesserAssociationByMongodbId(CCDetails['_id'])
        .subscribe(expenseData => {
          this.getCorporateContractTotalCountForPagination();
          this.getCorporateContractByType(this.corporateContractType)
        });
  }

  deleteTransactionData:any
  intermerdiateDeleteTranasaction(transactionData){
    this.deleteTransactionData=transactionData
  }

  getCorporateContractTotalCountForPagination() {
    this.vehicleCorporateContractService.getVehicleCorporateContractCount()
        .subscribe((ccCount: any) => {
          this.pagination.totalPageCount = ccCount.count;
        });
  }
  getCorporateContractBasedOnrange(skip, limit) {
    this.vehicleCorporateContractService.getAllVehicleCorporateContractList()
        .subscribe((corporateContractDetails:any) => {
          console.log("----------typeof corporateContractDetails")
          console.log( corporateContractDetails)
          console.log(typeof corporateContractDetails)
          this.corporateContractDetailsList=corporateContractDetails.reverse()
        });
  }
  getAllVechicleAsset(){
    this.assetService.getAssetlistSpecificList("Vehicle")
        .subscribe((vechicleList: any) => {
          this.vechicleDropdownList =vechicleList
        })
  }

  getAllLesserAsset(){
    this.assetService.getAssetlistSpecificList("Lesser")
        .subscribe((lesserList: any) => {
          this.lesserDropdownList =lesserList
        })
  }

  getAllCompanyAsset(){
    this.assetService.getAssetlistSpecificList("Lesser")
        .subscribe((companyList: any) => {
          this.lesserAssetDropDown =companyList
        })
  }

  getCorporateContractByType(ccType) {
    this.lesserAssociationService.getLesserAssociationByCCtype(ccType)
        .subscribe((corporateContractDetails:any) => {

          this.corporateContractDetailsList=corporateContractDetails.reverse()
        });
  }

    getAllCompanyOfTypeCC1(){
          this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
              .subscribe((companyList: any) => {
              console.log("IIIIIIIIIIIIIIIII")
              console.log(companyList)
                  this.companyAssetDropDown =_.uniqBy(companyList,function(companyList){ return companyList['companySelectedForDropDown']});
              })
      }


  ngOnInit() {
   this.getAllVechicleType()
   this.getAllCompanyAsset();
  this.getAllCompanyOfTypeCC1();
    this.getCorporateContractByType(this.corporateContractType)
    this.getCorporateContractTotalCountForPagination();
  }

  fetchCorporateConfigOnCompanySelected(){

  }


}
