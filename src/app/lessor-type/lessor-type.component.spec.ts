import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LessorTypeComponent} from './lessor-type.component';

describe('LessorTypeComponent', () => {
    let component: LessorTypeComponent;
    let fixture: ComponentFixture<LessorTypeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LessorTypeComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LessorTypeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
