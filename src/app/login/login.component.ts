import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginStatus: string;
    currRoute: string;
    email: string;
    password: string;

    constructor(private router: Router) {
        this.currRoute = router.url;
        console.log("this.router")
        console.log(this.router.url)
    }

    ngOnInit() {
    }


    forgotPasswordRoute(){
        this.router.navigate(['forgotPassword']);
    }

    login() {
        if (this.email == 'admin' && this.password == 'admin') {
            this.router.navigate(['lesserAsset']);
        } else {
            this.loginStatus = "Invalid Credentials"

        }
    }

}
