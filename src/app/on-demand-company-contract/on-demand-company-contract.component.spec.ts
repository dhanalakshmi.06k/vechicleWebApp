import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnDemandCompanyContractComponent } from './on-demand-company-contract.component';

describe('OnDemandCompanyContractComponent', () => {
  let component: OnDemandCompanyContractComponent;
  let fixture: ComponentFixture<OnDemandCompanyContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnDemandCompanyContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnDemandCompanyContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
