import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnDemandUsageReportComponent } from './on-demand-usage-report.component';

describe('OnDemandUsageReportComponent', () => {
  let component: OnDemandUsageReportComponent;
  let fixture: ComponentFixture<OnDemandUsageReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnDemandUsageReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnDemandUsageReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
