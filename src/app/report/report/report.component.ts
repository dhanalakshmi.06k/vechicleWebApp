import {Component, OnInit} from '@angular/core';
import * as _ from "lodash";
import {AssetService} from "../../../services/asset.service";
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {GenericTransactionReportService} from "../../../services/reportService/generic-transaction-report.service";
import {ExcelService} from "../../../services/excel.service";


@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
    showQueryResults:any={}
    reportQueryObj:any={}
    companyAssetList:any=[]
    driverAssetList:any=[]
    lesserAssetList:any=[]
    vehicleAssetList:any=[]
    vehicleTypeList:any=[]
    resultDetails:any=[]
    selectedVehicleType:any=""
    selectCompany:any=""
    selectedCCType:any=""
    individualDate:Date=null;
    startDate:Date=null;
    endDate:Date=null;
    reportHeader:any;
    ccList:any=["contract type 1","contract type 2","OnDemand","Shift Type","fixedDayKmContract"]
    constructor(public assetService: AssetService, public vechicleTypeServiceService:VechicleTypeServiceService,
               private excelService:ExcelService,
                 public genericTransactionReportService:GenericTransactionReportService) {
    }



    submitFetchQuery(){
                    let obj={}
                    obj['filter']=this.reportQueryObj
                    this.genericTransactionReportService.fetchTransactionDetails(obj)
                                .subscribe((reportList: any) => {
                                  this.reportHeader=Object.keys(reportList[0])
                                    console.log(reportList)
                                    this.resultDetails=reportList
                                     this.resetFrom()
                                })
            }


    resetFrom(){
              this.reportQueryObj={}
          }

    ngOnInit() {
        this.getAllVehicleType();
        this.getAlDriverAssets();
        this.getAllVendorAssets();
        this.getAllVehicleAssets();
        this.getAllAssets();
    }


    getAllVehicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)

                this.vehicleTypeList=vechicleTypes
            })
    }

    getAllAssets(){
        this.assetService.getAssetlistSpecificList( "Company" )
            .subscribe((assetList: any) => {
                this.companyAssetList=assetList
            })
    }


    getAlDriverAssets(){
        this.assetService.getAssetlistSpecificList( "Driver" )
            .subscribe((assetList: any) => {
                this.driverAssetList=assetList
            })
    }

    getAllVendorAssets(){
        this.assetService.getAssetlistSpecificList( "Lesser" )
            .subscribe((assetList: any) => {
                this.lesserAssetList=assetList
            })
    }

    getAllVehicleAssets(){
        this.assetService.getAssetlistSpecificList( "Vehicle" )
            .subscribe((assetList: any) => {
                this.vehicleAssetList=assetList
            })
    }

    downLoadPdf(){
        this.excelService.exportAsExcelFile(this.resultDetails, 'TRANSACTIONREPORT');
    }
}



