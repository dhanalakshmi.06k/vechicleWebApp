import {Component, OnInit} from '@angular/core';
import {ServiceTypeServiceService} from "../../services/service-type-service.service";
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {FormService} from "../../services/form.service";

@Component({
    selector: 'app-service-type',
    templateUrl: './service-type.component.html',
    styleUrls: ['./service-type.component.scss']
})
export class ServiceTypeComponent implements OnInit {
    public pagination: any;
    public mainAsset: any;
    public assetConfigParameterType: any;
    public assetId: any;
    public skip: number;
    public limit: number;
    public assetConfigName: any;
    public assetFromConfig: any;
    public allAssets: any = [];

    constructor(public serviceTypeServiceService: ServiceTypeServiceService, public applicationParameteConfigService: ApplicationParameteConfigService,
                public formService: FormService) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
    }


    updateAssetCardToAssetList(editAssetValue) {
        console.log("allAssetsallAssets00000000savedAssetValue00000000000")
        console.log(editAssetValue)
        this.getFuelTypeBasedOnrange(0, 10)
    }

    ngOnInit() {
        this.getFuelTypeBasedOnrange(0, 10)
        this.getAssetTotalCountForPagination()
    }

    savedAssetCardToAssetList(savedAssetValue) {
        console.log("allAssetsallAssets00000000savedAssetValue00000000000")
        console.log(savedAssetValue)
        this.getFuelTypeBasedOnrange(0, 10)
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getFuelTypeBasedOnrange(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getFuelTypeBasedOnrange(this.skip, this.limit);
        }
    }


    getEditConfiguration(assetDetails) {
        this.assetId = assetDetails._id
        this.applicationParameteConfigService.getApplicationParameterConfig("ServiceType")
            .subscribe((data: any) => {
                this.mainAsset = false;
                console.log(data.configuration)
                console.log(assetDetails)
                console.log(this.formService.formatEditAssetConfig(data.configuration, assetDetails))
                this.assetConfigParameterType = "ServiceType"
                // consolethis.configService.appConfig.appBaseUrl +'serviceDetails'.log(data)
                this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetDetails)

            });
    }

    deleteFuelTypeConfiguration(assetFuelDetails) {
        console.log("confog datae09000934-02943-0")
        console.log(assetFuelDetails._id)
        this.serviceTypeServiceService.deleteServiceTypeByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getFuelTypeBasedOnrange(0, 10)
            });
    }


    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("ServiceType")
            .subscribe((data: any) => {
                console.log("confog datae09000934-02943-0")
                console.log(data)
                this.mainAsset = false;
                this.assetConfigName = "ServiceType";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }

    getAssetTotalCountForPagination() {
        this.serviceTypeServiceService.getServiceTypeCount()
            .subscribe((serviceCount: any) => {
                console.log("allAssetsallAssets00000000savedAssetValue00000000000")
                console.log(serviceCount.count)
                this.pagination.totalPageCount = serviceCount.count;
            });
    }


    getFuelTypeBasedOnrange(skip, limit) {
        this.serviceTypeServiceService.getAllServiceType(skip, limit)
            .subscribe(allAssets => {
                console.log("this fuel type test listtttttttttttttttt")
                console.log(allAssets)
                this.allAssets = allAssets;

            });
    }

}
