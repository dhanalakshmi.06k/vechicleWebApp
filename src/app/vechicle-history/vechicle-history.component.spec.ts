import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VechicleHistoryComponent } from './vechicle-history.component';

describe('VechicleHistoryComponent', () => {
  let component: VechicleHistoryComponent;
  let fixture: ComponentFixture<VechicleHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VechicleHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VechicleHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
