import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {VechicleTransactionService} from "../../services/vechicle-transaction.service";
import {FormService} from "../../services/form.service";
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";
import {AssetService} from "../../services/asset.service";

@Component({
  selector: 'app-vechicle-transaction-form',
  templateUrl: './vechicle-transaction-form.component.html',
  styleUrls: ['./vechicle-transaction-form.component.scss']
})
export class VechicleTransactionFormComponent implements OnInit {
    @Output() savedAssetConfig: EventEmitter<any> = new EventEmitter();
    @Input() formConfigData: any;
    @Input() assetSelectedType: any;
    @Input() mainStatusAsset: any;
    @Input() mainAsset: any;
    shiftTypesDropDown:any;
    vechicleTypesDropDown:any;
    selectedShiftType:any;
    vechicleShiftType:any;
    vechicleDropdownList = [];
    selectedItems = [];
    vechicledropdownSettings = {};
    minDate = {year: 2018, month: 1, day: 1};
    maxDate = {year: 2018, month: 9, day: 16};
    dateSelected:any;



  constructor(public vechicleTransactionService:VechicleTransactionService,
              public formService:FormService,
              public shiftTypeServiceService: ShiftTypeServiceService,
              public vechicleTypeServiceService:VechicleTypeServiceService,
              public assetService:AssetService) { }

              ngOnInit() {
                  this.getAllVechicleAsset()
                  this.vechicledropdownSettings = {
                      singleSelection: false,
                      idField: '_id',
                      textField: 'name',
                      selectAllText: 'Select All',
                      unSelectAllText: 'UnSelect All',
                      itemsShowLimit: 1,
                      allowSearchFilter: true
                  };

      this.getAllShiftType();
      this.getAllVechicleType();

  }

    onItemSelect (item:any) {
        console.log("000000000000item-----------formConfigData");
        console.log(item._id);
        console.log(this.formConfigData);
        this.assetService.getAssetDetailsByID(item._id)
            .subscribe((assetData: any) => {
                console.log("--------getAssetDetailsByID---66666666666---")
                console.log(assetData)

            })

    }
    onSelectAll (items: any) {
        console.log(items);
    }
    onItemDeSelect(items:any){
        console.log(items)
    }

    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                console.log("--------vechicleList-----getAllShiftType--")
                console.log(vechicleList)
                this.vechicleDropdownList =vechicleList
            })
    }


    getAllShiftType(){
        this.shiftTypeServiceService.getShiftTypeCostDropDown()
            .subscribe((shiftTypes: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
                console.log(shiftTypes)
                this.shiftTypesDropDown=shiftTypes
            })
    }


    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log("--------shiftTypes-----getAllShiftType--")
                console.log(vechicleTypes)
                this.vechicleTypesDropDown=vechicleTypes
            })
    }

    submitTransactionData() {
        console.log("***savedAssetValue***********---assetFromData------this.formConfigData----on submit")
        console.log(this.dateSelected)
        this.formConfigData.assetType = this.assetSelectedType;

        let assetFromData=this.formService.formatAssetSaveDetails(this.formConfigData)
       assetFromData["mode"] = this.selectedShiftType;
       assetFromData["shiftType"]= this.vechicleShiftType;
        console.log("***savedAssetValue***********---assetFromData------this.formConfigData----on submit")
        console.log(assetFromData)
        this.vechicleTransactionService.saveVechicleTransaction(assetFromData)
            .subscribe((savedAssetValue: any) => {
                console.log("***savedAssetValue***********---------this.formConfigData----on submit")
                console.log(savedAssetValue)
                this.savedAssetConfig.emit(savedAssetValue);
                this.formConfigData=[]
            })

    }

}
