import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-vehicle-coporate-association-header',
  templateUrl: './vehicle-coporate-association-header.component.html',
  styleUrls: ['./vehicle-coporate-association-header.component.scss']
})
export class VehicleCoporateAssociationHeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

    navigateCCVehicle1() {
        this.router.navigate(['VehicleCorporateContract1']);
    }
    navigateCCVehicle2() {
        this.router.navigate(['VehicleCorporateContractShiftType2']);
    }
    navigateCommon() {
        this.router.navigate(['vehicleCorporateContractCommon']);
    }
    navigateOnDemandContract() {
        this.router.navigate(['companyOnDemandAssoication']);
    }
    navigateCCComapny2() {
        this.router.navigate(['companyCorporateContract2']);
    }

}
