import { Component, OnInit } from '@angular/core';
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";
import {VechicleExpenseServiceService} from "../../services/vechicle-expense-service.service";
import {FormService} from "../../services/form.service";
import {AssetService} from "../../services/asset.service";
import {ReportServiceService} from "../../services/reportService/report-service.service";
import {ExcelService} from "../../services/excel.service";

@Component({
  selector: 'app-vehicle-expense',
  templateUrl: './vehicle-expense.component.html',
  styleUrls: ['./vehicle-expense.component.scss']
})
export class VehicleExpenseComponent implements OnInit {
    allAssets:any=[]
    expenseEditDetails:any
    vechicleExpenseObj:any={


    }
    public pagination: any;
    vehicleNo:any;
    expenseFromField:any;
    expenseDescription:any;
    expenseSelectionType:any="Spare Parts";
    expenseType:any;
    fitnessCertificate:any;
    repairOption:any;
    expenseDate:any;
    dataListHeader:any
    showEditFrom:boolean=false;
    allVehicleAssets:any=[]


  constructor(public vechicleExpenseServiceService: VechicleExpenseServiceService,
              public formService:FormService ,public assetService:AssetService,private excelService:ExcelService ){

      this.pagination = {
          'currentPage': 1,
          'totalPageCount': 0,
          'recordsPerPage': 12
      };
  }


    downLoadPdf(){
        let assetListReport=[]
        for(let p=0;p<this.allAssets.length;p++){
            assetListReport.push(this.allAssets[p]['vehicleExpenseDetails'])
        }
        this.excelService.exportAsExcelFile(assetListReport, 'EXPENASEREPORT');
    }

    getVechicleList(){
      this.expenseFromField=[]
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.allVehicleAssets =vechicleList
            })
    }

    getDataBasedOnSelection(){
        this.expenseSelectionType = this.expenseSelectionType.replace(/ +/g, "");
        this.vechicleExpenseServiceService.getVechicleExpenseBasedOnExpenseType(this.expenseSelectionType)
            .subscribe((resultExpense:any) => {
                if(resultExpense.length>0){
                    this.dataListHeader=Object.keys(resultExpense[0]['vehicleExpenseDetails'])
                    for(let i=0;i<resultExpense.length;i++){
                        console.log("******resultExpense*****getDataBasedOnSelection********");
                        console.log(resultExpense[i])
                    }
                    this.allAssets =  resultExpense;
                }
                if(resultExpense.length===0){
                    this.allAssets =  [];
                }

            });
    }




    submitExpenseDetails(){
        this.expenseSelectionType = this.expenseType;
        var obj={}
        obj["vehicleNo"]=this.vehicleNo
        obj["expenseType"]=this.expenseType
        obj["expenseDescription"]=this.expenseDescription
        obj["expenseDate"]=this.expenseDate
        obj["vehicleExpenseDetails"]  =this.formService.formatAssetSaveDetails(this.expenseFromField)
        this.vechicleExpenseServiceService.saveVechicleExpense(obj)
            .subscribe((data: any) => {
                this.getAssetTotalCountForPagination()
                this.getDataBasedOnSelection()
            });
    }

  ngOnInit() {
      this.getAssetTotalCountForPagination()
      this.getDataBasedOnSelection()
  }

    getAssetTotalCountForPagination() {
        this.vechicleExpenseServiceService.getVechicleExpenseCount()
            .subscribe((userCount: any) => {
                this.pagination.totalPageCount = userCount.count;
            });
    }

    getExpenseBasedOnrange(skip, limit) {
        this.vechicleExpenseServiceService.getVechicleExpense(skip, limit)
            .subscribe(allAssets => {
                console.log("allAssets1111111111111111111")
                console.log(allAssets[0])
               /* this.dataListHeader=Object.keys(allAssets[0]['vehicleExpenseDetails'])
                console.log("allAssets111dataListHeader1111111 this.dataListHeader??????????/!**!/11dataListHeader1111111")
                console.log(this.dataListHeader)
                this.allAssets =   allAssets;*/

            });
    }

    restructureResponse(allAssets){
      for(var j=0;j<allAssets.length;j++){
          console.log("allAssets1111111111111111111")
          console.log(allAssets[j].expenseDate)
          const obj = allAssets[j].expenseDate
          let arr = [];
          for (let p in obj)
              arr.push(obj[p]);
          const str = arr.join('-');
          console.log("0000000000000000000")
          console.log(str)
          allAssets[j]['modifieDate'] = str
      }
      return allAssets;

    }

    getEditConfiguration(expenseDetails){
        this.vechicleExpenseServiceService.getVechicleExpenseByMongodbId(expenseDetails._id)
            .subscribe(expenseData => {
                this.showEditFrom=true
                this.expenseEditDetails=expenseData
            });
    }


    updateExpenseDetails(expenseDetails){
        this.vechicleExpenseServiceService.updateVechicleExpenseById(expenseDetails._id,expenseDetails)
            .subscribe(expenseData => {
                this.showEditFrom=true
                this.expenseEditDetails=expenseData
                this.getAssetTotalCountForPagination()
                this.getExpenseBasedOnrange(0, 10)
            });
    }

    deleteExpenseDetails(expenseDetails){
      console.log("?????????????expenseDetails")
        console.log(expenseDetails)
        this.expenseSelectionType=expenseDetails['expenseType']
        this.vechicleExpenseServiceService.deleteVechicleExpenseByMongodbId(expenseDetails['_id'])
            .subscribe(expenseData => {
                /*this.getAssetTotalCountForPagination()*/
                this.getDataBasedOnSelection()
            });

    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }


    getFromFields(){
        this.expenseType = this.expenseType.replace(/ +/g, "");
         let expenseFrom=this.formService.getExpenseTypefromConfig();
        console.log(expenseFrom)

        if(this.expenseType!=='FitnessCertificate' && this.expenseType!=='Repair'){
            for(let h=0;h<expenseFrom.length;h++){
                if(expenseFrom[h].assetType.typeId===this.expenseType){
                    let configObj=  expenseFrom[h].configuration
                    let responseKeys = Object.keys(configObj);
                    let formData = [];
                    for ( var prop of responseKeys) {
                        formData.push( configObj[prop]);
                    }
                    this.expenseFromField=  formData
                }
            }
        }
        if(this.expenseType==='FitnessCertificate' && this.expenseType==='Repair' ){
            this.expenseFromField=  []
        }
    }

    getFitnessFromFields(){
        this.fitnessCertificate = this.fitnessCertificate.replace(/ +/g, "");
        let fitnessCertificateFrom=this.formService.getExpenseTypefromConfig();
        for(let h=0;h<fitnessCertificateFrom.length;h++){
            if(fitnessCertificateFrom[h].assetType.typeId===this.fitnessCertificate){
                let configObj=  fitnessCertificateFrom[h].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.expenseFromField=  formData
            }
        }

    }
    getRepairFromFields(){
        this.repairOption = this.repairOption.replace(/ +/g, "");
        let repairOptionFrom=this.formService.getExpenseTypefromConfig();
        for(let h=0;h<repairOptionFrom.length;h++){
            if(repairOptionFrom[h].assetType.typeId===this.repairOption){
                let configObj=  repairOptionFrom[h].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.expenseFromField=  formData
            }
        }

    }


}
