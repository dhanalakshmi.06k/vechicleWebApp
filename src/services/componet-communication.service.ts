import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";

@Injectable()
export class ComponetCommunicationService {

    public subject = new Subject<any>();

    constructor() { }

    setCCSelected(ccTYpe:String) {
        this.subject.next({ ccTYpe: ccTYpe });
    }


    getCCSelected(): Observable<any> {
        return this.subject.asObservable();
    }

}
