import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../config.service";

@Injectable()
export class GenericTransactionReportService {

   constructor(private http: HttpClient, public configService: ConfigService) {}
  fetchTransactionDetails(queryObj) {
          return this.http.post(this.configService.appConfig.appBaseUrl + 'getReportDetails', queryObj);
      }

}
