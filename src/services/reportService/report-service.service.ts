import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../config.service";

@Injectable()
export class ReportServiceService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }
    getIndividualDayReport(date) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReportByDay/?currentDate='+date);
    }

    getRangeByReport(startDate,endDate) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReportByDateRange/?startDate='+startDate+'&endDate='+endDate);
    }

    getReportByCompanyName(cctype,companyName) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReportByCompanyNameAndCCType/?cctype='+cctype+'&companyName='+companyName);
    }

    getReportByOnlyCompanyName(companyName) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReportByCompanyName/?companyName='+companyName);
    }
    getReportByCompanyNameAndVehicleType(companyName,vehicleType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReportByCompanyNameAndVehicleType/?companyName='+companyName+'&vehicleType='+vehicleType);
    }

    getReportByOnlyCCType(ccType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReportOnlyByCCType/?ccType='+ccType);
    }

    getReportByCompanyNameAndVehicleTypeAndCCType(companyName,vehicleType,ccType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReportByCompanyNameAndVehicleTypeAndccType/?companyName='+companyName+'&vehicleType='+vehicleType+'&ccType='+ccType);
    }

    getCorporateContractReport(companyName,cctype,startDate,endDate,vehicleType) {
        if(companyName!==""){
            return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReport/?companyName='+companyName);
        }
        if(cctype!=="") {
            return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReport/?cctype='+cctype);
        }
        if(cctype!=="" && companyName!=="" ) {
            return this.http.get(this.configService.appConfig.appBaseUrl + 'transactionReport/?cctype='+cctype+'&companyName='+companyName);
        }



    }
}
