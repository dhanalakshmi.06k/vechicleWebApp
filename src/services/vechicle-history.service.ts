import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class VechicleHistoryService {

  constructor(private http: HttpClient, public configService: ConfigService) { }

  saveVechicleHistoryDetails(vechicleData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'vechicleHistoryDetails', vechicleData);
    }

    getAllVechicleHistory(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allVechicleHistoryDetails' + '/' + skip + '/' + limit);
    }

    getVechicleHistoryByMongodbId(vechicleMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vechicleHistoryBymongoId/' + vechicleMongoId);
    }

    updateVechicleHistoryById(vechicleMongoId, details) {
        console.log("----------------responseee")
        console.log(vechicleMongoId)
        console.log(details)
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editvechicleHistoryBymongoId/' + vechicleMongoId, details);
    }


    deleteVechicleHistoryByMongodbId(vechicleMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'vechicleHistoryBymongoId/' + vechicleMongoId);
    }

    getVechicleHistoryCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vechicleHistory/count');
    }




}
