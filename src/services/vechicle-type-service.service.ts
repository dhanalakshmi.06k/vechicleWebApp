import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class VechicleTypeServiceService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    getVechicleTypeCostCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'VehicleDetails/count');
    }


    saveVechicleTypeDetails(vehicleTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'vehicleDetails', vehicleTypeData);
    }

    getAllVechicleType(start,limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allVehicleTypeDetails/'  + start+'/'+limit);
    }

    getVechicleTypeByMongodbId(vehicleMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vehicleBymongoId/' + vehicleMongoId);
    }

    updateVechicleTypeById(typeId,vechicleTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editVehicleBymongoId/'+typeId, vechicleTypeData);
    }


    deleteVechicleTypeByMongodbId(vehicleMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteVehicleBymongoId/' + vehicleMongoId);
    }

    getAllVechicleTypeDropdown() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allVehicleTypeForDropDown' );
    }
    getAllVechicle() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetsByAssetType/'+"Vehicle" );
    }

}
