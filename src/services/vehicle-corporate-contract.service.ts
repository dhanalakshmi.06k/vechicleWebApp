import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class VehicleCorporateContractService {

  constructor(private http: HttpClient, public configService: ConfigService) { }

    saveVehicleCorporateContract(corporateContractData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'vehicleCorporateContract', corporateContractData);
    }

    getVehicleCorporateContractByCCType(CCType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allvehicleCorporateContractByContractType' + '/' + CCType );
    }

    getAllVehicleCorporateContract(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vehicleCorporateContract' + '/' + skip + '/' + limit);
    }

    getAllVehicleCorporateContractList() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allvehicleCorporateContract' );
    }

    getVehicleCorporateContractByMongodbId(vehicleCorporateContractMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vehicleCorporateContractBymongoId/' + vehicleCorporateContractMongoId);
    }

    getCompanyCorporateContractByMongodbId(companyMongoDbID) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'companyCorporateContractByMongodbId/' + companyMongoDbID);
    }

    getcompanyCCByMongodbIdAndCCType(companyMongoDbID,ccType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'companyCCByMongodbIdAndCCType/' + companyMongoDbID
        +'/'+ccType);
    }
    updateVehicleCorporateContractById(vehicleCorporateContractMongoId, details) {

        return this.http.post(this.configService.appConfig.appBaseUrl + 'editVehicleCorporateContract/' + vehicleCorporateContractMongoId, details);
    }


    deleteVehicleCorporateContractByMongodbId(vehicleCorporateContractMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'vehicleCorporateContractBymongoId/' + vehicleCorporateContractMongoId);
    }

    getVehicleCorporateContractCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vehicleCorporateContract/count');
    }

    getCompanyByCCType(CCType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'comapnyCorporateContract/' + CCType);
    }
    checkCCExitsBasedOnVehicleTypeAndName(companyName,ccType ,vehicleType){
        return this.http.get(this.configService.appConfig.appBaseUrl + 'getccBasedOnNameAndType/' + companyName+'/'+ccType+'/'+vehicleType);
    }

    getcompanyCCByMongodbIdAndCCTypeAndvehicleType(companyMongoDbID,ccType,vehicleType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'companyCCByMongodbIdAndCCTypeAndVehicleType/' + companyMongoDbID
            +'/'+ccType +'/'+vehicleType);
    }


    getcompanyCCByCompanyAndCCTypeAndVehicleType(companyName,ccType,vehicleType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'companyCCByCompanyNameAndCCTypeAndVehicleType/' + companyName
            +'/'+ccType +'/'+vehicleType);
    }

    getcompanyCCByCompanyAndCCTypeAndVehicleTypeAndShiftType(companyName,ccType,vehicleType,shiftTYpe) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'companyCCByCompanyNameAndCCTypeAndVehicleTypeAndShiftType/' + companyName
            +'/'+ccType +'/'+vehicleType+'/'+shiftTYpe);
    }

    getcompanyCCByMongodbIdAndCCTypeAndvehicleTypeandShiftType(companyMongoDbID,ccType,vehicleType,shiftType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'companyCCByMongodbIdAndCCTypeAndVehicleTypeAndShiftType/' + companyMongoDbID
            +'/'+ccType +'/'+vehicleType+'/'+shiftType);
    }


    getCorporateContractByCCTypeRange(CCType,skip,limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vehicleCorporateContractByContractTypeByRange' + '/' + CCType + '/' + skip+ '/' + limit);
    }

    getCorporateContractByCCTypeCount(CCType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vehicleCorporateContractByContractCount' + '/' + CCType );
    }



    //vehicleContractByLeasedType/:lesserName/:ccType/:vehicleType/:companyName
    getContractDetailsByLeasedType(lesserName,ccType,vhType,companyName) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vehicleContractByLeasedType'
            + '/' + lesserName + '/' + ccType + '/' + vhType + '/' + companyName );
    }

        //vehicleContractByOwnedType/:vehicleType/:ccType/:companyName
    getContractDetailsByOwnedType(vhType,ccType,companyName) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vehicleContractByOwnedType' + '/' + vhType  + '/' + ccType  + '/' + companyName );
    }

}
